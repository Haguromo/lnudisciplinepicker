﻿namespace DisciplinePicker.BusinessLogic
{
    using DisciplinePicker.BusinessLogic.Contracts;
    using DisciplinePicker.BusinessLogic.Services;
    using Microsoft.Extensions.DependencyInjection;

    public static class BusinessLogicServices
    {
        public static void ConfigureBusinessLogic(this IServiceCollection services)
        {
            services.AddTransient<IDisciplineService, DisciplineService>();
        }
    }
}
