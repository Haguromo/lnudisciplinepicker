﻿namespace DisciplinePicker.BusinessLogic.Services
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using DisciplinePicker.BusinessLogic.Contracts;
    using DisciplinePicker.Web.Models;

    class DisciplineService : IDisciplineService
    {
        private ApplicationContext _context;
        
        public DisciplineService(ApplicationContext context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<string>> GetAllFaculties()
        {
            var disciplines = _context.Disciplines;
            var faculties = disciplines.Select(x => x.Faculty).Distinct();
            return await faculties.ToListAsync();
        }

        public async Task<IEnumerable<Discipline>> GetDisciplines(
            string query,
            string faculty,
            DisciplineSortOrder sortOrder,
            int semester
            )
        {
            var availabilities = _context.DisciplineAvailabilities.Include("Discipline");
            var currentYear = DateTime.Now.Year;
            availabilities = availabilities.Where(x => x.Semester == semester);
            if(semester == 1)
            {
                availabilities = availabilities.Where(x => x.Year == currentYear);
            }
            else
            {
                availabilities = availabilities.Where(x => x.Year == currentYear+1);
            }
            
            availabilities = availabilities.Where(x => x.MaxAllovedStudents > x.DisciplineChoises.Count);

            var disciplines = availabilities.Select(x => x.Discipline);

            if (!string.IsNullOrWhiteSpace(query))
            {
                disciplines = disciplines.Where(x => x.Name.Contains(query));
            }
            if (!string.IsNullOrWhiteSpace(faculty))
            {
                disciplines = disciplines.Where(x => x.Faculty.ToLower() == faculty.ToLower());
            }
            switch (sortOrder)
            {
                case DisciplineSortOrder.Faculty:
                    disciplines = disciplines.OrderBy(x => x.Faculty);
                    break;
                case DisciplineSortOrder.Name:
                default:
                    disciplines = disciplines.OrderBy(x => x.Name);
                    break;
            }

            return await disciplines.ToListAsync();
        }
        
    }
}
