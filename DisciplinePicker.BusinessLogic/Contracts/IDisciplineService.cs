﻿namespace DisciplinePicker.BusinessLogic.Contracts
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DisciplinePicker.Web.Models;

    public interface IDisciplineService
    {
        Task<IEnumerable<Discipline>> GetDisciplines(string query, string faculty, DisciplineSortOrder sortOrder, int semester);
        Task<IEnumerable<string>> GetAllFaculties();
    }
}
