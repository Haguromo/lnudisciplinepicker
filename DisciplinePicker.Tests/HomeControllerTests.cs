using DisciplinePicker.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using Xunit;

namespace DisciplinePicker.Tests
{
    public class HomeControllerTests
    {
        [Fact]
        public void IndexViewResultNotNull()
        {
            // Arrange
            HomeController controller = new HomeController();
            // Act
            ViewResult result = controller.Index() as ViewResult;
            // Assert
            Assert.NotNull(result);
        }
    }
}
