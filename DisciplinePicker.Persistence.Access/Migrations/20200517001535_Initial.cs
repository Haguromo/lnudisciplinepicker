﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DisciplinePicker.Persistence.Infrastructure.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Disciplines",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Department = table.Column<string>(nullable: true),
                    Faculty = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Disciplines", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lecturers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Department = table.Column<string>(nullable: true),
                    Faculty = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lecturers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DisciplineAvailabilities",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Year = table.Column<int>(nullable: false),
                    Semester = table.Column<int>(nullable: false),
                    Hours = table.Column<int>(nullable: false),
                    MaxAllovedStudents = table.Column<int>(nullable: false),
                    MinAllovedStudents = table.Column<int>(nullable: false),
                    DisciplineId = table.Column<Guid>(nullable: false),
                    LecturerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DisciplineAvailabilities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Discipline_DisciplineAvailabilities",
                        column: x => x.DisciplineId,
                        principalTable: "Disciplines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Lecturer_DisciplineAvailabilities",
                        column: x => x.LecturerId,
                        principalTable: "Lecturers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Login = table.Column<string>(nullable: true),
                    Group = table.Column<string>(nullable: true),
                    Course = table.Column<int>(nullable: false),
                    Speciality = table.Column<string>(nullable: true),
                    Department = table.Column<string>(nullable: true),
                    Photo = table.Column<byte[]>(nullable: true),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Students_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DisciplineChoises",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DisciplineAvailabilityId = table.Column<Guid>(nullable: false),
                    StudentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DisciplineChoises", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DisciplineChoise_DisciplineAvailability",
                        column: x => x.DisciplineAvailabilityId,
                        principalTable: "DisciplineAvailabilities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DisciplineChoise_Student",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Disciplines",
                columns: new[] { "Id", "Department", "Description", "Faculty", "Name" },
                values: new object[,]
                {
                    { new Guid("ccee00a9-7fa0-4454-bc92-a6712fa2dd31"), "Дискретного аналізу та інтелектуальних систем", "Найкращий предмет на світі", "Прикладної математики та інформатики", "Системи штучного інтелекту" },
                    { new Guid("a8a206d6-88e7-4a65-ad07-1ed1ae8ba26c"), "БЖД", "Потрібний предмет", "", "БЖД" },
                    { new Guid("7c69c55d-9664-49f5-a649-9296e28bb4b1"), "Математичного та функціонального аналізу", "границі, інтеграли, похідні, ряди", "Механіко-математичний", "Математичний аналіз" },
                    { new Guid("6953c2fd-abed-403f-bd83-3adee9f18a1b"), "Прикладної філософії", "у чому сенс життя", "Філософський", "Філософія" },
                    { new Guid("668520ac-6f5c-46bd-9697-4328f3f4dd43"), "Слизерин", "варити зілля", "Гоґвортс", "Зіллєваріння" },
                    { new Guid("dbde8f57-eb7c-4037-b4d7-df63d1693dcf"), "Програмування", "Патерни", "Прикладної математики та інформатики", "Програмна інженерія" }
                });

            migrationBuilder.InsertData(
                table: "Lecturers",
                columns: new[] { "Id", "Department", "Faculty", "Name" },
                values: new object[,]
                {
                    { new Guid("a59deb47-bdd7-40c2-856d-6c5f82d190da"), "Математичного та функціонального аналізу", "Мех-мат", "Тарасюк Святослав" },
                    { new Guid("f305918a-ccab-422c-8e4b-f3e252749fcd"), "Інформаційних систем", "Прикладна математика", "Юрій Щербина" },
                    { new Guid("a86b40f6-f89e-4756-81a1-8c0e72425bb7"), "Програмування", "Прикладна математика", "Анатолій Музичук" },
                    { new Guid("1844b523-d4a2-422e-8ac1-cfc6320bc1d7"), "Української філології", "Філологічний", "Іван Іваненко" },
                    { new Guid("58e681f8-36f3-4a6c-805d-02101fdd2fac"), "Англійської філології", "Філологічний", "Петро Петренко" },
                    { new Guid("3e857a39-c9f3-4330-92cf-c7db64251004"), "Поезії", "Поетичний", "Тарас Шевченко" }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("2432fe24-3a95-4bfe-85bd-4191cd41b230"), "admin" },
                    { new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), "student" }
                });

            migrationBuilder.InsertData(
                table: "DisciplineAvailabilities",
                columns: new[] { "Id", "DisciplineId", "Hours", "LecturerId", "MaxAllovedStudents", "MinAllovedStudents", "Semester", "Year" },
                values: new object[,]
                {
                    { new Guid("a264f312-ea65-407a-a277-f114a4e29129"), new Guid("a8a206d6-88e7-4a65-ad07-1ed1ae8ba26c"), 90, new Guid("a59deb47-bdd7-40c2-856d-6c5f82d190da"), 200, 25, 5, 2020 },
                    { new Guid("eed646e6-92eb-47fd-8bf5-2a2470f0cbef"), new Guid("ccee00a9-7fa0-4454-bc92-a6712fa2dd31"), 90, new Guid("f305918a-ccab-422c-8e4b-f3e252749fcd"), 200, 25, 7, 2020 },
                    { new Guid("ccaf9b3e-39f7-4450-bf39-a0aaf15a024f"), new Guid("ccee00a9-7fa0-4454-bc92-a6712fa2dd31"), 900, new Guid("f305918a-ccab-422c-8e4b-f3e252749fcd"), 2090, 25, 2, 2020 },
                    { new Guid("4face143-8133-4ada-85ad-4287bde9b030"), new Guid("668520ac-6f5c-46bd-9697-4328f3f4dd43"), 90, new Guid("f305918a-ccab-422c-8e4b-f3e252749fcd"), 2, 2, 6, 2020 },
                    { new Guid("50fb5ff4-435e-44a5-9fe3-a4fae1a5c927"), new Guid("ccee00a9-7fa0-4454-bc92-a6712fa2dd31"), 90, new Guid("a86b40f6-f89e-4756-81a1-8c0e72425bb7"), 200, 25, 6, 2020 },
                    { new Guid("932c3fd2-35da-4118-8ae8-301fe69ed8b2"), new Guid("a8a206d6-88e7-4a65-ad07-1ed1ae8ba26c"), 100, new Guid("a86b40f6-f89e-4756-81a1-8c0e72425bb7"), 200, 29, 4, 2021 }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "Id", "Course", "Department", "Group", "Login", "Name", "Password", "Photo", "RoleId", "Speciality" },
                values: new object[,]
                {
                    { new Guid("f8cdc96a-b2a7-4486-bd22-681f64c47b23"), 4, "Фізичний", "Ph22", "okean@gmail.com", "Святослав Вакарчук", "elzy", null, new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), "Співак" },
                    { new Guid("3cf9a078-fdff-4a49-b3f8-98e5fb704389"), 1, "Мехмат", "MMM22", "my_energy@gmail.com", "Стефан Банах", "dyiak", null, new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), "Математик" },
                    { new Guid("9d3e378b-889e-4871-b6ad-c34ac598fe06"), 4, "Хімічний", "Chem22", "fedko@gmail.com", "Андрій Федько", "fedko$#@", null, new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), "Хімік" },
                    { new Guid("7bb37e34-041e-4098-b1b5-65023f6c2f1f"), 2, "Прикладної математики", "PMP22", "porokh@gmail.com", "Богдан Порохнавець", "armiia_mova_vira", null, new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), "Математик" },
                    { new Guid("349c5197-4d38-439e-b536-4b0c1b3fdcce"), 6, "Міжнародних відносин", "Presidents5", "porokh@gmail.com", "Петро Порошенко", "armiia_mova_vira", null, new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), "Президент" },
                    { new Guid("3fb6a621-256f-4c92-8f21-8e725590f749"), 3, "Інформаційних підсистем", "Gp50", "А@gmail.com (А@gmail.com)", "Лютер Кінг", "1234", null, new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), "Комп'ютерні монітори" },
                    { new Guid("2c3126ba-9a18-437d-bd8c-0f8f3870726b"), 3, "Інформаційних систем", "Gp39", "M@gmail.com", "Щерба Максим", "123456aA_", null, new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), "Комп'ютерні науки" },
                    { new Guid("a3ced0f8-7d7a-4442-871e-5bcd401e74a3"), 2, "Інформаційних систем нейронних", "Gp40", "А@gmail.com (А@gmail.com)", "Альфа Дельта", "1234", null, new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), "Комп'ютерні науки богарта" },
                    { new Guid("bcb4de5c-b017-4359-bc1d-e7cdd01c9004"), 3, "Юридичний", "Ju1", "andrew@gmail.com", "Андрій Вдовіцин", "superPassword", null, new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), "Суддя" },
                    { new Guid("3344352b-e75c-4a27-801b-3f451871b942"), 3, "Інформаційних систем", "Gp38", "B@gmail.com", "Власюк Василь", "123456aA_", null, new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), "Комп'ютерні науки" },
                    { new Guid("37bc7c3c-74aa-4e74-9273-3fd54a3984a7"), 3, "Інформаційних систем", "Gp37", "bozhook@gmail.com", "Віктор Божук", "123456aA_", null, new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), "Комп'ютерні науки" },
                    { new Guid("1f444245-51e5-452a-bdab-6010e82b8937"), 0, null, null, "admin.com", null, "123", null, new Guid("2432fe24-3a95-4bfe-85bd-4191cd41b230"), null },
                    { new Guid("d7b727f8-373b-4145-8cd9-f0ec0d09d098"), 1, "Риторичних систем нейронних", "Gp42", "В@gmail.com (В@gmail.com)", "Браво Фокстрот", "1234", null, new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), "Міщанські науки" },
                    { new Guid("43fdafa1-88a8-4538-9327-145109a02586"), 2, "Дискретного аналізу та інтелектуальних систем", "AMi25", "chb19@gmail.com", "Богдан Чіх", "armiia_mova_vira", null, new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), "Програміст" }
                });

            migrationBuilder.InsertData(
                table: "DisciplineChoises",
                columns: new[] { "Id", "DisciplineAvailabilityId", "StudentId" },
                values: new object[,]
                {
                    { new Guid("afe0387f-45a2-4c17-9ae4-a7a01798ad05"), new Guid("50fb5ff4-435e-44a5-9fe3-a4fae1a5c927"), new Guid("37bc7c3c-74aa-4e74-9273-3fd54a3984a7") },
                    { new Guid("a8a206d6-88e7-4a65-ad07-1ed1ae8ba26c"), new Guid("eed646e6-92eb-47fd-8bf5-2a2470f0cbef"), new Guid("37bc7c3c-74aa-4e74-9273-3fd54a3984a7") },
                    { new Guid("668520ac-6f5c-46bd-9697-4328f3f4dd43"), new Guid("ccaf9b3e-39f7-4450-bf39-a0aaf15a024f"), new Guid("3344352b-e75c-4a27-801b-3f451871b942") },
                    { new Guid("4face143-8133-4ada-85ad-4287bde9b030"), new Guid("ccaf9b3e-39f7-4450-bf39-a0aaf15a024f"), new Guid("3344352b-e75c-4a27-801b-3f451871b942") },
                    { new Guid("3b23a1c5-106e-4938-9bcb-13a1ad9cfb9b"), new Guid("eed646e6-92eb-47fd-8bf5-2a2470f0cbef"), new Guid("3344352b-e75c-4a27-801b-3f451871b942") },
                    { new Guid("432a2e67-bc7e-4399-ae56-b8198647d036"), new Guid("50fb5ff4-435e-44a5-9fe3-a4fae1a5c927"), new Guid("3344352b-e75c-4a27-801b-3f451871b942") },
                    { new Guid("50fb5ff4-435e-44a5-9fe3-a4fae1a5c927"), new Guid("50fb5ff4-435e-44a5-9fe3-a4fae1a5c927"), new Guid("3344352b-e75c-4a27-801b-3f451871b942") },
                    { new Guid("ccaf9b3e-39f7-4450-bf39-a0aaf15a024f"), new Guid("a264f312-ea65-407a-a277-f114a4e29129"), new Guid("2c3126ba-9a18-437d-bd8c-0f8f3870726b") },
                    { new Guid("7c69c55d-9664-49f5-a649-9296e28bb4b1"), new Guid("a264f312-ea65-407a-a277-f114a4e29129"), new Guid("d7b727f8-373b-4145-8cd9-f0ec0d09d098") },
                    { new Guid("932c3fd2-35da-4118-8ae8-301fe69ed8b2"), new Guid("4face143-8133-4ada-85ad-4287bde9b030"), new Guid("349c5197-4d38-439e-b536-4b0c1b3fdcce") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_DisciplineAvailabilities_DisciplineId",
                table: "DisciplineAvailabilities",
                column: "DisciplineId");

            migrationBuilder.CreateIndex(
                name: "IX_DisciplineAvailabilities_LecturerId",
                table: "DisciplineAvailabilities",
                column: "LecturerId");

            migrationBuilder.CreateIndex(
                name: "IX_DisciplineChoises_DisciplineAvailabilityId",
                table: "DisciplineChoises",
                column: "DisciplineAvailabilityId");

            migrationBuilder.CreateIndex(
                name: "IX_DisciplineChoises_StudentId",
                table: "DisciplineChoises",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_RoleId",
                table: "Students",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DisciplineChoises");

            migrationBuilder.DropTable(
                name: "DisciplineAvailabilities");

            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Disciplines");

            migrationBuilder.DropTable(
                name: "Lecturers");

            migrationBuilder.DropTable(
                name: "Roles");
        }
    }
}
