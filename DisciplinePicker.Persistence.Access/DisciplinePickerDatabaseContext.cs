﻿// <copyright file="ApplicationContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System;
using DisciplinePicker.Persistence.Model;
using Microsoft.EntityFrameworkCore;

namespace DisciplinePicker.Persistence.Infrastructure
{
    /// <summary>
    /// Context class for connection to data base.
    /// </summary>
    public class DisciplinePickerDatabaseContext : DbContext
    {
        public DisciplinePickerDatabaseContext()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DisciplinePickerDatabaseContext"/> class.
        /// </summary>
        /// <param name="options">DataBase context options.</param>
        public DisciplinePickerDatabaseContext(DbContextOptions<DisciplinePickerDatabaseContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets disciplines collection.
        /// </summary>
        public virtual DbSet<Discipline> Disciplines { get; set; }

        /// <summary>
        /// Gets or sets DisciplineAvailabilities collection.
        /// </summary>
        public virtual DbSet<DisciplineAvailability> DisciplineAvailabilities { get; set; }

        /// <summary>
        /// Gets or sets Lecturers collection.
        /// </summary>
        public virtual DbSet<Lecturer> Lecturers { get; set; }

        /// <summary>
        /// Gets or sets Students collection.
        /// </summary>
        public virtual DbSet<Student> Students { get; set; }

        /// <summary>
        /// Gets or sets DisciplineChoises collection.
        /// </summary>
        public virtual DbSet<DisciplineChoise> DisciplineChoises { get; set; }

        /// <summary>
        /// Gets or sets Roles collection.
        /// </summary>
        public DbSet<Role> Roles { get; set; }

        /// <summary>
        /// The method launch when data base is creating.
        /// </summary>
        /// <param name="modelBuilder">param.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            string adminRoleName = "admin";
            string studentRoleName = "student";

            string adminEmail = "admin.com";
            string adminPassword = "123";
            
            Role adminRole = new Role { Id = new Guid("2432fe24-3a95-4bfe-85bd-4191cd41b230"), Name = adminRoleName };
            Role studentRole = new Role { Id = new Guid("442d3c5c-8737-4934-bac9-f20f35e99a88"), Name = studentRoleName };
            Student adminUser = new Student { Id = new Guid("1f444245-51e5-452a-bdab-6010e82b8937"), Login = adminEmail, Password = adminPassword, RoleId = adminRole.Id };

            modelBuilder.Entity<Role>().HasData(new Role[] { adminRole, studentRole });
            modelBuilder.Entity<Student>().HasData(new Student[] { adminUser });

            // modelBuilder.Entity<Discipline>(entity =>
            // {
            //    entity.Property(e => e.Name)
            //        .IsRequired();
            //    entity.Property(e => e.Department)
            //        .IsRequired();
            //    entity.Property(e => e.Description)
            //        .IsRequired()
            //        .HasColumnType("text");
            //    entity.Property(e => e.Faculty)
            //        .IsRequired();
            // });

            // modelBuilder.Entity<Student>(entity =>
            // {
            //    entity.Property(e => e.Department)
            //        .IsRequired();
            //    entity.Property(e => e.Group)
            //        .IsRequired();
            //    entity.Property(e => e.Speciality)
            //        .IsRequired();
            // });

            // modelBuilder.Entity<Lecturer>(entity =>
            // {
            //    entity.Property(e => e.Department)
            //        .IsRequired();
            //    entity.Property(e => e.Faculty)
            //        .IsRequired();
            // });
            modelBuilder.Entity<DisciplineAvailability>(entity =>
            {
                // entity.Property(e => e.Year)
                //    .IsRequired();

                // entity.Property(e => e.Semester)
                //    .IsRequired();

                // entity.Property(e => e.Hours)
                //    .IsRequired();

                // entity.Property(e => e.MaxAllovedStudents)
                //    .IsRequired();

                // entity.Property(e => e.MinAllovedStudents)
                //    .IsRequired();
                entity.HasOne(d => d.Discipline)
                    .WithMany(p => p.DisciplineAvailabilities)
                    .HasForeignKey(d => d.DisciplineId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Discipline_DisciplineAvailabilities");

                entity.HasOne(d => d.Lecturer)
                    .WithMany(p => p.DisciplineAvailabilities)
                    .HasForeignKey(d => d.LecturerId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Lecturer_DisciplineAvailabilities");
            });

            modelBuilder.Entity<DisciplineChoise>(entity =>
            {
                // entity.Property(e => e.DisciplineAvailabilityId)
                //    .IsRequired();

                // entity.Property(e => e.StudentId)
                //    .IsRequired();
                entity.HasOne(d => d.DisciplineAvailability)
                    .WithMany(p => p.DisciplineChoises)
                    .HasForeignKey(d => d.DisciplineAvailabilityId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_DisciplineChoise_DisciplineAvailability");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.DisciplineChoises)
                    .HasForeignKey(d => d.StudentId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_DisciplineChoise_Student");
            });

            Student student1 = new Student()
            {
                Id = new Guid("37bc7c3c-74aa-4e74-9273-3fd54a3984a7"),
                Name = "Віктор Божук",
                Login = "bozhook@gmail.com",
                Password = "123456aA_",
                Course = 3,
                Group = "Gp37",
                Department = "Інформаційних систем",
                Speciality = "Комп'ютерні науки",
                RoleId = studentRole.Id,
            };

            Student student2 = new Student()
            {
                Id = new Guid("3344352b-e75c-4a27-801b-3f451871b942"),
                Name = "Власюк Василь",
                Login = "B@gmail.com",
                Password = "123456aA_",
                Course = 3,
                Group = "Gp38",
                Department = "Інформаційних систем",
                Speciality = "Комп'ютерні науки",
                RoleId = studentRole.Id,
            };
            Student student3 = new Student()
            {
                Id = new Guid("2c3126ba-9a18-437d-bd8c-0f8f3870726b"),
                Name = "Щерба Максим",
                Login = "M@gmail.com",
                Password = "123456aA_",
                Course = 3,
                Group = "Gp39",
                Department = "Інформаційних систем",
                Speciality = "Комп'ютерні науки",
                RoleId = studentRole.Id,
            };

            Student student4 = new Student()
            {
                Id = new Guid("a3ced0f8-7d7a-4442-871e-5bcd401e74a3"),
                Name = "Альфа Дельта",
                Login = "А@gmail.com (А@gmail.com)",
                Password = "1234",
                Course = 2,
                Group = "Gp40",
                Department = "Інформаційних систем нейронних",
                Speciality = "Комп'ютерні науки богарта",
                RoleId = studentRole.Id,
            };

            Student student5 = new Student()
            {
                Id = new Guid("d7b727f8-373b-4145-8cd9-f0ec0d09d098"),
                Name = "Браво Фокстрот",
                Login = "В@gmail.com (В@gmail.com)",
                Password = "1234",
                Course = 1,
                Group = "Gp42",
                Department = "Риторичних систем нейронних",
                Speciality = "Міщанські науки",
                RoleId = studentRole.Id,
            };

            Student student6 = new Student()
            {
                Id = new Guid("3fb6a621-256f-4c92-8f21-8e725590f749"),
                Name = "Лютер Кінг",
                Login = "А@gmail.com (А@gmail.com)",
                Password = "1234",
                Course = 3,
                Group = "Gp50",
                Department = "Інформаційних підсистем",
                Speciality = "Комп'ютерні монітори",
                RoleId = studentRole.Id,
            };

            Student student7 = new Student()
            {
                Id = new Guid("349c5197-4d38-439e-b536-4b0c1b3fdcce"),
                Name = "Петро Порошенко",
                Login = "porokh@gmail.com",
                Password = "armiia_mova_vira",
                Course = 6,
                Group = "Presidents5",
                Department = "Міжнародних відносин",
                Speciality = "Президент",
                RoleId = studentRole.Id,
            };

            Student student13 = new Student()
            {
                Id = new Guid("43fdafa1-88a8-4538-9327-145109a02586"),
                Name = "Богдан Чіх",
                Login = "chb19@gmail.com",
                Password = "armiia_mova_vira",
                Course = 2,
                Group = "AMi25",
                Department = "Дискретного аналізу та інтелектуальних систем",
                Speciality = "Програміст",
                RoleId = studentRole.Id,
            };

            Student student8 = new Student()
            {
                Id = new Guid("7bb37e34-041e-4098-b1b5-65023f6c2f1f"),
                Name = "Богдан Порохнавець",
                Login = "porokh@gmail.com",
                Password = "armiia_mova_vira",
                Course = 2,
                Group = "PMP22",
                Department = "Прикладної математики",
                Speciality = "Математик",
                RoleId = studentRole.Id,
            };

            Student student9 = new Student()
            {
                Id = new Guid("9d3e378b-889e-4871-b6ad-c34ac598fe06"),
                Name = "Андрій Федько",
                Login = "fedko@gmail.com",
                Password = "fedko$#@",
                Course = 4,
                Group = "Chem22",
                Department = "Хімічний",
                Speciality = "Хімік",
                RoleId = studentRole.Id,
            };

            Student student10 = new Student()
            {
                Id = new Guid("3cf9a078-fdff-4a49-b3f8-98e5fb704389"),
                Name = "Стефан Банах",
                Login = "my_energy@gmail.com",
                Password = "dyiak",
                Course = 1,
                Group = "MMM22",
                Department = "Мехмат",
                Speciality = "Математик",
                RoleId = studentRole.Id,
            };

            Student student11 = new Student()
            {
                Id = new Guid("f8cdc96a-b2a7-4486-bd22-681f64c47b23"),
                Name = "Святослав Вакарчук",
                Login = "okean@gmail.com",
                Password = "elzy",
                Course = 4,
                Group = "Ph22",
                Department = "Фізичний",
                Speciality = "Співак",
                RoleId = studentRole.Id,
            };

            Student student12 = new Student()
            {
                Id = new Guid("bcb4de5c-b017-4359-bc1d-e7cdd01c9004"),
                Name = "Андрій Вдовіцин",
                Login = "andrew@gmail.com",
                Password = "superPassword",
                Course = 3,
                Group = "Ju1",
                Department = "Юридичний",
                Speciality = "Суддя",
                RoleId = studentRole.Id,
            };



            Discipline discipline1 = new Discipline()
            {
                Id = new Guid("ccee00a9-7fa0-4454-bc92-a6712fa2dd31"),
                Name = "Системи штучного інтелекту",
                Department = "Дискретного аналізу та інтелектуальних систем",
                Description = "Найкращий предмет на світі",
                Faculty = "Прикладної математики та інформатики",
            };
            Discipline discipline2 = new Discipline()
            {
                Id = new Guid("a8a206d6-88e7-4a65-ad07-1ed1ae8ba26c"),
                Name = "БЖД",
                Department = "БЖД",
                Description = "Потрібний предмет",
                Faculty = "",
            };
            Discipline discipline3 = new Discipline()
            {
                Id = new Guid("7c69c55d-9664-49f5-a649-9296e28bb4b1"),
                Name = "Математичний аналіз",
                Department = "Математичного та функціонального аналізу",
                Description = "границі, інтеграли, похідні, ряди",
                Faculty = "Механіко-математичний",
            };

            Discipline discipline4 = new Discipline()
            {
                Id = new Guid("6953c2fd-abed-403f-bd83-3adee9f18a1b"),
                Name = "Філософія",
                Department = "Прикладної філософії",
                Description = "у чому сенс життя",
                Faculty = "Філософський",
            };

            Discipline discipline5 = new Discipline()
            {
                Id = new Guid("668520ac-6f5c-46bd-9697-4328f3f4dd43"),
                Name = "Зіллєваріння",
                Department = "Слизерин",
                Description = "варити зілля",
                Faculty = "Гоґвортс",
            };

            Discipline discipline6 = new Discipline()
            {
                Id = new Guid("dbde8f57-eb7c-4037-b4d7-df63d1693dcf"),
                Name = "Програмна інженерія",
                Department = "Програмування",
                Description = "Патерни",
                Faculty = "Прикладної математики та інформатики",
            };

            Lecturer lecturer1 = new Lecturer()
            {
                Id = new Guid("a59deb47-bdd7-40c2-856d-6c5f82d190da"),
                Name = "Тарасюк Святослав",
                Department = "Математичного та функціонального аналізу",
                Faculty = "Мех-мат",
            };
            Lecturer lecturer2 = new Lecturer()
            {
                Id = new Guid("f305918a-ccab-422c-8e4b-f3e252749fcd"),
                Name = "Юрій Щербина",
                Department = "Інформаційних систем",
                Faculty = "Прикладна математика",
            };
            Lecturer lecturer3 = new Lecturer()
            {
                Id = new Guid("a86b40f6-f89e-4756-81a1-8c0e72425bb7"),
                Name = "Анатолій Музичук",
                Department = "Програмування",
                Faculty = "Прикладна математика",
            };
            Lecturer lecturer4 = new Lecturer()
            {
                Id = new Guid("1844b523-d4a2-422e-8ac1-cfc6320bc1d7"),
                Name = "Іван Іваненко",
                Department = "Української філології",
                Faculty = "Філологічний",
            };
            Lecturer lecturer5 = new Lecturer()
            {
                Id = new Guid("58e681f8-36f3-4a6c-805d-02101fdd2fac"),
                Name = "Петро Петренко",
                Department = "Англійської філології",
                Faculty = "Філологічний",
            };
            Lecturer lecturer6 = new Lecturer()
            {
                Id = new Guid("3e857a39-c9f3-4330-92cf-c7db64251004"),
                Name = "Тарас Шевченко",
                Department = "Поезії",
                Faculty = "Поетичний",
            };

            modelBuilder.Entity<Student>().HasData(new Student[] { student1, student2, student3, student4, student5, student6,
                student7, student8, student9, student10, student11, student12, student13 });
            modelBuilder.Entity<Discipline>().HasData(new Discipline[] { discipline1, discipline2, discipline3, discipline4, discipline5, discipline6 });
            modelBuilder.Entity<Lecturer>().HasData(new Lecturer[] { lecturer1, lecturer2, lecturer3, lecturer4, lecturer5, lecturer6 });

            DisciplineAvailability dis1 = new DisciplineAvailability()
            {
                Id = new Guid("50fb5ff4-435e-44a5-9fe3-a4fae1a5c927"),
                Semester = 6,
                Year = 2020,
                Hours = 90,
                MaxAllovedStudents = 200,
                MinAllovedStudents = 25,
                DisciplineId = discipline1.Id,
                LecturerId = lecturer3.Id,
            };

            DisciplineAvailability dis2 = new DisciplineAvailability()
            {
                Id = new Guid("eed646e6-92eb-47fd-8bf5-2a2470f0cbef"),
                Semester = 7,
                Year = 2020,
                Hours = 90,
                MaxAllovedStudents = 200,
                MinAllovedStudents = 25,
                DisciplineId = discipline1.Id,
                LecturerId = lecturer2.Id,
            };

            DisciplineAvailability dis3 = new DisciplineAvailability()
            {
                Id = new Guid("a264f312-ea65-407a-a277-f114a4e29129"),
                Semester = 5,
                Year = 2020,
                Hours = 90,
                MaxAllovedStudents = 200,
                MinAllovedStudents = 25,
                DisciplineId = discipline2.Id,
                LecturerId = lecturer1.Id,
            };

            DisciplineAvailability dis4 = new DisciplineAvailability()
            {
                Id = new Guid("932c3fd2-35da-4118-8ae8-301fe69ed8b2"),
                Semester = 4,
                Year = 2021,
                Hours = 100,
                MaxAllovedStudents = 200,
                MinAllovedStudents = 29,
                DisciplineId = discipline2.Id,
                LecturerId = lecturer3.Id,
            };

            DisciplineAvailability dis5 = new DisciplineAvailability()
            {
                Id = new Guid("ccaf9b3e-39f7-4450-bf39-a0aaf15a024f"),
                Semester = 2,
                Year = 2020,
                Hours = 900,
                MaxAllovedStudents = 2090,
                MinAllovedStudents = 25,
                DisciplineId = discipline1.Id,
                LecturerId = lecturer2.Id,
            };

            DisciplineAvailability dis6 = new DisciplineAvailability()
            {
                Id = new Guid("4face143-8133-4ada-85ad-4287bde9b030"),
                Semester = 6,
                Year = 2020,
                Hours = 90,
                MaxAllovedStudents = 2,
                MinAllovedStudents = 2,
                DisciplineId = discipline5.Id,
                LecturerId = lecturer2.Id,
            };

            DisciplineChoise dc1 = new DisciplineChoise() { Id = new Guid("afe0387f-45a2-4c17-9ae4-a7a01798ad05"), DisciplineAvailabilityId = dis1.Id, StudentId = student1.Id };
            DisciplineChoise dc2 = new DisciplineChoise() { Id = new Guid("a8a206d6-88e7-4a65-ad07-1ed1ae8ba26c"), DisciplineAvailabilityId = dis2.Id, StudentId = student1.Id };
            DisciplineChoise dc3 = new DisciplineChoise() { Id = new Guid("668520ac-6f5c-46bd-9697-4328f3f4dd43"), DisciplineAvailabilityId = dis5.Id, StudentId = student2.Id };
            DisciplineChoise dc4 = new DisciplineChoise() { Id = new Guid("7c69c55d-9664-49f5-a649-9296e28bb4b1"), DisciplineAvailabilityId = dis3.Id, StudentId = student5.Id };
            DisciplineChoise dc5 = new DisciplineChoise() { Id = new Guid("932c3fd2-35da-4118-8ae8-301fe69ed8b2"), DisciplineAvailabilityId = dis6.Id, StudentId = student7.Id };
            DisciplineChoise dc6 = new DisciplineChoise() { Id = new Guid("4face143-8133-4ada-85ad-4287bde9b030"), DisciplineAvailabilityId = dis5.Id, StudentId = student2.Id };
            DisciplineChoise dc7 = new DisciplineChoise() { Id = new Guid("ccaf9b3e-39f7-4450-bf39-a0aaf15a024f"), DisciplineAvailabilityId = dis3.Id, StudentId = student3.Id };
            DisciplineChoise dc8 = new DisciplineChoise() { Id = new Guid("3b23a1c5-106e-4938-9bcb-13a1ad9cfb9b"), DisciplineAvailabilityId = dis2.Id, StudentId = student2.Id };
            DisciplineChoise dc9 = new DisciplineChoise() { Id = new Guid("432a2e67-bc7e-4399-ae56-b8198647d036"), DisciplineAvailabilityId = dis1.Id, StudentId = student2.Id };
            DisciplineChoise dc10 = new DisciplineChoise() { Id = new Guid("50fb5ff4-435e-44a5-9fe3-a4fae1a5c927"), DisciplineAvailabilityId = dis1.Id, StudentId = student2.Id };

            modelBuilder.Entity<DisciplineAvailability>().HasData(new DisciplineAvailability[] { dis1, dis2, dis3, dis4, dis5, dis6 });
            modelBuilder.Entity<DisciplineChoise>().HasData(new DisciplineChoise[] { dc1, dc2, dc3, dc4, dc5, dc6, dc7, dc8, dc9, dc10 });
        }
    }
}
