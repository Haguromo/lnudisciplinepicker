﻿using DisciplinePicker.Persistence.Model;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace DisciplinePicker.Persistence.Infrastructure.Repository
{
    public interface IDisciplineAvailabilityStorage
    {
        Task<DisciplineAvailability> GetAvailability(string availabilityId);
        Task EditAvailability(string availabilityId, AvailabilityArgs args);
        Task AddNewAvailability(AvailabilityArgs args);
        Task DeleteAvailability(string availabilityId);
        Task<List<DisciplineAvailability>> GetAvailabilities(Student student);
        Task<List<DisciplineAvailability>> GetAvailabilities(Lecturer lecturer);
        Task<List<DisciplineAvailability>> GetAvailabilities();
    }

    public class AvailabilityArgs
    {
        public int Hours { get; set; }
        public int Year { get; set; }
        public int Semester { get; set; }
        public int MinAllovedStudents { get; set; }
        public int MaxAllovedStudents { get; set; }
        public string DisciplineId { get; set; }
        public string LecturerId { get; set; }
        public string Lecturer { get; set; }
        public string NameDiscipline { get; set; }
    }
}
