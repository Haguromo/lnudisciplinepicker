﻿using DisciplinePicker.Persistence.Model;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace DisciplinePicker.Persistence.Infrastructure.Repository
{
    public interface IDisciplineStorage
    {
        Task AddDiscipline(DisciplineArgs args);
        Task<Discipline> GetDiscipline(string disciplineId);
        Task<Discipline> GetDisciplineByName(string name);
        Task EditDiscipline(string disciplineId, DisciplineArgs args);
        Task DeleteDiscipline(string disciplineId);
        Task<List<Discipline>> GetAllDisciplines();
    }

    public class DisciplineArgs
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Faculty { get; set; }
        public string Department { get; set; }
    }
}
