﻿using DisciplinePicker.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DisciplinePicker.Persistence.Infrastructure.Repository
{
    public interface IStudentStorage
    {
        Task<Student> GetStudent(string studentId);
        Task<Student> GetStudentByLogin(string login);
        Task AddNewStudent(StudentArgs args);
        Task EditStudent(string studentId, StudentArgs args);
        Task DeleteStudent(string studentId);
        Task ChangeStudentsPassword(string studentId, string newPassword);
        Task<List<Student>> GetStudents(string role);
        Task<List<Student>> GetStudents(Guid availabilityId);
    }

    public class StudentArgs
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public int Course { get; set; }
        public string Department { get; set; }
        public string Group { get; set; }
        public string Name { get; set; }
        public string Speciality { get; set; }
    }
}
