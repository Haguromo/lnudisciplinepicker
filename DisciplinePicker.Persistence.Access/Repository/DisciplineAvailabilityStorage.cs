﻿using DisciplinePicker.Persistence.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisciplinePicker.Persistence.Infrastructure.Repository
{
    public class DisciplineAvailabilityStorage : IDisciplineAvailabilityStorage
    {
        private DisciplinePickerDatabaseContext _context;

        public DisciplineAvailabilityStorage(DisciplinePickerDatabaseContext context)
        {
            _context = context;
        }

        public async Task AddNewAvailability(AvailabilityArgs args)
        {
            if (args == null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            var discipline = await _context.Disciplines.FirstOrDefaultAsync(x => x.Id.ToString() == args.DisciplineId);
            if (discipline == null)
            {
                throw new ArgumentException("Discipline not found", nameof(args));
            }

            var lecturer = await _context.Lecturers.FirstOrDefaultAsync(x => x.Id.ToString() == args.LecturerId);
            if (discipline == null)
            {
                throw new ArgumentException("Lecturer not found", nameof(args));
            }

            var availability = new DisciplineAvailability
            {
                Discipline = discipline,
                Lecturer = lecturer,
                Hours = args.Hours,
                MaxAllovedStudents = args.MaxAllovedStudents,
                MinAllovedStudents = args.MinAllovedStudents,
                Semester = args.Semester,
                Year = args.Year
            };
            await _context.DisciplineAvailabilities.AddAsync(availability);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAvailability(string availabilityId)
        {
            var availability = await _context.DisciplineAvailabilities.FirstOrDefaultAsync(x => x.Id.ToString() == availabilityId);
            if (availability != null)
            {
                _context.DisciplineAvailabilities.Remove(availability);
                await _context.SaveChangesAsync();
            }
        }

        public async Task EditAvailability(string availabilityId, AvailabilityArgs args)
        {
            if (args == null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            var discipline = await _context.Disciplines.FirstOrDefaultAsync(x => x.Id.ToString() == args.DisciplineId);
            if (discipline == null)
            {
                throw new ArgumentException("Discipline not found", nameof(args));
            }

            var lecturer = await _context.Lecturers.FirstOrDefaultAsync(x => x.Id.ToString() == args.LecturerId);
            if (discipline == null)
            {
                throw new ArgumentException("Lecturer not found", nameof(args));
            }

            var availability = await _context.DisciplineAvailabilities.FirstOrDefaultAsync(x => x.Id.ToString() == availabilityId);
            if (availability != null)
            {
                availability.Discipline = discipline;
                availability.Lecturer = lecturer;
                availability.Semester = args.Semester;
                availability.MaxAllovedStudents = args.MaxAllovedStudents;
                availability.MinAllovedStudents = args.MinAllovedStudents;
                availability.Hours = args.Hours;
                availability.Year = args.Year;

                _context.DisciplineAvailabilities.Update(availability);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<DisciplineAvailability> GetAvailability(string availabilityId)
        {
            return await _context.DisciplineAvailabilities.Include("Discipline").Include("Lecturer").Include(x => x.DisciplineChoises)
                .ThenInclude(x => x.Student).FirstOrDefaultAsync(x => x.Id.ToString() == availabilityId);
        }

        public async Task<List<DisciplineAvailability>> GetAvailabilities(Student student)
        {
            return await _context.DisciplineAvailabilities.Include(x => x.DisciplineChoises).ThenInclude(x => x.Student)
                .Include(x => x.Lecturer).Include(x => x.Discipline).Where(x => x.DisciplineChoises.Where(p => p.Student == student).Any()).ToListAsync();
        }

        public async Task<List<DisciplineAvailability>> GetAvailabilities(Lecturer lecturer)
        {
            return await _context.DisciplineAvailabilities.Include(x => x.DisciplineChoises).ThenInclude(x => x.Student)
                .Include(x => x.Lecturer).Include(x => x.Discipline).Where(x => x.Lecturer == lecturer).ToListAsync();
        }

        public async Task<List<DisciplineAvailability>> GetAvailabilities()
        {
            return await _context.DisciplineAvailabilities.Include("Discipline").Include("Lecturer").Include(x => x.DisciplineChoises).ThenInclude(x => x.Student).ToListAsync();
        }

        public DisciplineAvailability GetById(Guid id)
        {
            return _context.DisciplineAvailabilities.FirstOrDefault(x => x.Id == id);
        }


    }
}
