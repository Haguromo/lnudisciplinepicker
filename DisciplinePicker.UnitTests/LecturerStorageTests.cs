﻿using DisciplinePicker.Persistence.Infrastructure.Repository;
using DisciplinePicker.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DisciplinePicker.UnitTests
{
    public class LecturerStorageTests : TestWithSqlite
    {
        [Fact]
        public async Task LecturerCountShoudBeEnlargedWhenAddedNewLecturer()
        {
            var lecturerStorage = new LecturerStorage(DbContext);
            var initialLecturerCount = DbContext.Lecturers.Count();
            var lecturerArgs = new NewLecturerArgs()
            {
                Name = "saqqw",
                Department = "adw",
                Faculty = "wew"
            };
            await lecturerStorage.AddLecturer(lecturerArgs);

            Assert.Equal(initialLecturerCount + 1, DbContext.Lecturers.Count());
        }
        [Fact]
        public async Task LecturerCountShoudBeReducedWhenDeletedLecturer()
        {
            var lecturerStorage = new LecturerStorage(DbContext);
            var lecturerArgs = new NewLecturerArgs()
            {
                Name = "saqqw",
                Department = "adw",
                Faculty = "wew"
            };
            await lecturerStorage.AddLecturer(lecturerArgs);
            var initialLecturerCount = DbContext.Lecturers.Count();
            var lecturerToDelete = lecturerStorage.GetLecturerByName(lecturerArgs.Name);

            await lecturerStorage.DeleteLecturer(lecturerToDelete.Result.Id.ToString());

            Assert.Equal(initialLecturerCount - 1, DbContext.Lecturers.Count());
        }

        [Fact]
        public async Task LecturerShouldBeEditedWhenEditedLecturer()
        {
            var lecturerStorage = new LecturerStorage(DbContext);
            var firstLecturerArgs = new NewLecturerArgs()
            {
                Name = "saqqw",
                Department = "adw",
                Faculty = "wew"
            };
            await lecturerStorage.AddLecturer(firstLecturerArgs);

            var lecturerId = DbContext.Lecturers.FirstOrDefault(x => x.Name == firstLecturerArgs.Name).Id;
            var secondLecturerArgs = new NewLecturerArgs()
            {
                Name = "saqqw1qaf",
                Department = "adw1aq",
                Faculty = "wewawqe"
            };
            await lecturerStorage.EditLecturer(lecturerId.ToString(), secondLecturerArgs);

            Assert.Equal(secondLecturerArgs.Name, DbContext.Lecturers.FirstOrDefault(x => x.Id == lecturerId).Name);
        }

        [Fact]
        public void LecturerShoudBeReturnedWhenGetedLecturerByName()
        {
            var lecturerName = DbContext.Lecturers.First().Name;
            var lecturerStorage = new LecturerStorage(DbContext);
            var lecturer = DbContext.Lecturers.FirstOrDefault(x => x.Name == lecturerName);
            var newLecturer = lecturerStorage.GetLecturerByName(lecturerName).Result;

            Assert.Equal(lecturer, newLecturer);
        }
    }
}
