﻿using DisciplinePicker.Persistence.Infrastructure.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

using Xunit;

namespace DisciplinePicker.UnitTests
{
    public class DisciplineAvailabilitiesStorageTests : TestWithSqlite
    {
        [Fact]
        public async Task DisciplineAvailabilityShouldBeInDatabaseWhenAddedNew()
        {
            var availabilitiesStorage = new DisciplineAvailabilityStorage(DbContext);
            var discipline = DbContext.Disciplines.First();
            var relatedLecturer = DbContext.Lecturers.First();
            var initialSize = DbContext.DisciplineAvailabilities.Count();

            await availabilitiesStorage.AddNewAvailability(new AvailabilityArgs()
            {
                DisciplineId = discipline.Id.ToString(),
                Hours = 90,
                Year = 2021,
                Semester = 6,
                MinAllovedStudents = 50,
                MaxAllovedStudents = 150,
                LecturerId = relatedLecturer.Id.ToString()
            });

            Assert.Equal(initialSize + 1, DbContext.DisciplineAvailabilities.Count());
            
        }

        [Fact]
        public async Task DisciplineAvailabilityShouldNotBeInDatabaseWhenRemoved()
        {
            var availabilitiesStorage = new DisciplineAvailabilityStorage(DbContext);
            
            var initialSize = DbContext.DisciplineAvailabilities.Count();
            var removedAvailability = DbContext.DisciplineAvailabilities.First();
            
            await availabilitiesStorage.DeleteAvailability(removedAvailability.Id.ToString());

            Assert.False(DbContext.DisciplineAvailabilities.Contains(removedAvailability));
        }

        [Fact]
        public async Task DisciplineAvailabilityStorageSizeShouldBeReducedWhenDeleted()
        {
            var availabilitiesStorage = new DisciplineAvailabilityStorage(DbContext);

            var initialSize = DbContext.DisciplineAvailabilities.Count();
            var removedAvailability = DbContext.DisciplineAvailabilities.First();

            await availabilitiesStorage.DeleteAvailability(removedAvailability.Id.ToString());

            Assert.Equal(initialSize - 1, DbContext.DisciplineAvailabilities.Count());
        }

        [Fact]
        public async Task DisciplineAvailabilitiesShouldBeEditedWhenEditedAvailability()
        {
            var availabilitiesStorage = new DisciplineAvailabilityStorage(DbContext);

            var targetDiscipline = DbContext.DisciplineAvailabilities.First();

            var newMaxAllovedCount = targetDiscipline.MaxAllovedStudents + 50;
            targetDiscipline.MaxAllovedStudents = newMaxAllovedCount;

            var availabilityArgs = new AvailabilityArgs()
            {
                DisciplineId = targetDiscipline.DisciplineId.ToString(),
                Hours = targetDiscipline.Hours,
                Year = targetDiscipline.Year,
                Semester = targetDiscipline.Year,
                MinAllovedStudents = targetDiscipline.MinAllovedStudents,
                MaxAllovedStudents = targetDiscipline.MaxAllovedStudents,
                LecturerId = targetDiscipline.LecturerId.ToString()
            };

            await availabilitiesStorage.EditAvailability(targetDiscipline.Id.ToString(),availabilityArgs);

            Assert.Equal(newMaxAllovedCount, DbContext.DisciplineAvailabilities.First(x => x.Id == targetDiscipline.Id).MaxAllovedStudents);
        }

        [Fact]
        public async Task DisciplineAvailabilityShouldBeReturnedWhenGeted()
        {
            var availabilitiesStorage = new DisciplineAvailabilityStorage(DbContext);
            var availability = DbContext.DisciplineAvailabilities.First();

            var foundAvailability = await availabilitiesStorage.GetAvailability(availability.Id.ToString());
            var foundAvailability2 = availabilitiesStorage.GetById(availability.Id);

            Assert.Equal(availability.Id.ToString(), foundAvailability.Id.ToString());
            Assert.Equal(availability.Id.ToString(), foundAvailability2.Id.ToString());
        }

        [Fact]
        public async Task DisciplineAvailabilitiesShouldBeReturnedAllWhenGeted()
        {
            var availabilitiesStorage = new DisciplineAvailabilityStorage(DbContext);
            var allAvailabilities = await DbContext.DisciplineAvailabilities.ToListAsync();
            allAvailabilities = allAvailabilities.OrderBy(x => x.Id).ToList();

            var acquiredDisciplines = await availabilitiesStorage.GetAvailabilities();
            acquiredDisciplines = acquiredDisciplines.OrderBy(x => x.Id).ToList();
            
            Assert.Equal(allAvailabilities, acquiredDisciplines);
        }

        [Fact]
        public async Task DisciplinesAvailabilitiesShouldBeReturnedFilled()
        {
            var availabilitiesStorage = new DisciplineAvailabilityStorage(DbContext);

            var availabilities = await availabilitiesStorage.GetAvailabilities();

            Assert.NotNull(availabilities.First().Lecturer);
            Assert.NotNull(availabilities.First().Discipline);
            Assert.NotNull(availabilities.First().DisciplineChoises);
        }
    }
}
