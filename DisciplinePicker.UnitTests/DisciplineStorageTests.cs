﻿using DisciplinePicker.Persistence.Infrastructure.Repository;

using System;
using System.Linq;
using System.Threading.Tasks;

using Xunit;

namespace DisciplinePicker.UnitTests
{
    public class DisciplineStorageTests : TestWithSqlite
    {
        [Fact]
        public async Task DisciplinesCountShoudBeEnlargedWhenAddedNewDiscipline()
        {
            var disciplineStorage = new DisciplineStorage(DbContext);
            var initialDisciplinesCount = DbContext.Disciplines.Count();
            var createDisciplineArgs = new DisciplineArgs
            {
                 Name = "Фехтування",
                 Faculty = "Бойові мистецтва",
                 Department = "Ближній бій",
                 Description = "Бій у нашому серці.",
            };

            await disciplineStorage.AddDiscipline(createDisciplineArgs);

            Assert.Equal(initialDisciplinesCount + 1, DbContext.Disciplines.Count());
        }

        [Fact]
        public async Task DisciplinesCountShoudBeReducedWhenDeletedDiscipline()
        {
            var disciplineId = DbContext.Disciplines.First().Id;
            var disciplineStorage = new DisciplineStorage(DbContext);
            var disciplinesCount = DbContext.Disciplines.Count();
            await disciplineStorage.DeleteDiscipline(disciplineId.ToString());

            Assert.Equal(disciplinesCount - 1, DbContext.Disciplines.Count());
        }

        [Fact]
        public async Task DisciplineShoudNotBeInTheDataBaseWhenDeletedDiscipline()
        {
            var disciplineId = DbContext.Disciplines.First().Id;
            var disciplineStorage = new DisciplineStorage(DbContext);
            var disciplinesCount = DbContext.Disciplines.Count();
            var discipline = DbContext.Disciplines.First();
            await disciplineStorage.DeleteDiscipline(disciplineId.ToString());

            Assert.False(DbContext.Disciplines.Contains(discipline));
        }

        [Fact]
        public async Task DisciplineShoudBeEditedWhenEditedDiscipline()
        {
            var disciplineId = DbContext.Disciplines.First().Id;
            var faculty = "Слизарин";
            var disciplineStorage = new DisciplineStorage(DbContext);
            var disciplinesCount = DbContext.Disciplines.Count();
            var discipline = DbContext.Disciplines.First(x => x.Id == disciplineId);
            var disciplineArgs = new DisciplineArgs()
            {
                Faculty = faculty,
            };

            await disciplineStorage.EditDiscipline(disciplineId.ToString(), disciplineArgs);

            Assert.Equal(faculty, DbContext.Disciplines.First(x => x.Id == disciplineId).Faculty);
        }

        [Fact]
        public async Task DisciplineShoudBeReturnedWhenGetedDiscipline()
        {
            var disciplineId = DbContext.Disciplines.First().Id;
            var disciplineStorage = new DisciplineStorage(DbContext);
            var discipline = DbContext.Disciplines.First();

            var newDiscipline = await disciplineStorage.GetDiscipline(disciplineId.ToString());

            Assert.Equal(discipline, newDiscipline);
        }

        [Fact]
        public async Task DisciplinesShoudBeReturnedEverythingWhenGetedAllDiscipline()
        {
            var disciplineStorage = new DisciplineStorage(DbContext);
            var allDisciplines = DbContext.Disciplines;

            var newDisciplines = await disciplineStorage.GetAllDisciplines();

            Assert.Equal(allDisciplines, newDisciplines);
        }
    }
}
