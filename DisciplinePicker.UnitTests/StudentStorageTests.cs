﻿using DisciplinePicker.Persistence.Infrastructure.Repository;

using System;
using System.Linq;
using System.Threading.Tasks;

using Xunit;

namespace DisciplinePicker.UnitTests
{
    public class StudentStorageTests : TestWithSqlite
    {
        [Fact]
        public async Task StudentsCountShoudBeEnlargedWhenAddedNewStudent()
        {
            var studentStorage = new StudentStorage(DbContext);
            var initialStudentsCount = DbContext.Students.Count();
            var createStudentArgs = new StudentArgs
            {
                Course = 11,
                Department = "aa",
                Group = "dsd",
                Login = "Maks121",
                Password = "1233rt",
                Name = "Maks",
                Speciality = "dsd"
            };

            await studentStorage.AddNewStudent(createStudentArgs);

            Assert.Equal(initialStudentsCount + 1, DbContext.Students.Count());
        }

        [Fact]
        public async Task StudentShoudBeInTheCollectionWhenAddedNewStudent()
        {
            var studentStorage = new StudentStorage(DbContext);
            var initialStudentsCount = DbContext.Students.Count();
            var createStudentArgs = new StudentArgs
            {
                Course = 11,
                Department = "aa",
                Group = "dsd",
                Login = "Maks121",
                Password = "1233rt",
                Name = "Maks",
                Speciality = "dsd"
            };

            await studentStorage.AddNewStudent(createStudentArgs);
            var newStudent = DbContext.Students.First(x => x.Login == createStudentArgs.Login && x.Password == createStudentArgs.Password);
            Assert.True(DbContext.Students.Contains(newStudent));
        }

        [Fact]
        public async Task StudentPasswordShoudBeEditedWhenEditedPassword()
        {
            var studentId = new Guid("3b4e5908-7842-4523-9ea6-bb14befb3d0e");
            var newPassword = "123456789";
            var studentStorage = new StudentStorage(DbContext);
            await studentStorage.ChangeStudentsPassword(studentId.ToString(), newPassword);
            var newStudent = DbContext.Students.FirstOrDefault(x => x.Id == studentId);
            Assert.Equal(newStudent.Password, newPassword);
        }

        [Fact]
        public async Task StudentsCountShoudBeReducedWhenDeletedStudent()
        {
            var studentId = new Guid("3b4e5908-7842-4523-9ea6-bb14befb3d0e");
            var studentStorage = new StudentStorage(DbContext);
            var studentsCount = DbContext.Students.Count();
            await studentStorage.DeleteStudent(studentId.ToString());

            Assert.Equal(DbContext.Students.Count(), studentsCount - 1);
        }

        [Fact]
        public async Task StudentShoudNotBeInTheDataBaseWhenDeletedStudent()
        {
            var studentId = new Guid("3b4e5908-7842-4523-9ea6-bb14befb3d0e");
            var studentStorage = new StudentStorage(DbContext);
            var student = DbContext.Students.FirstOrDefault(x => x.Id == studentId);
            await studentStorage.DeleteStudent(studentId.ToString());

            Assert.False(DbContext.Students.Contains(student));
        }

        [Fact]
        public async Task StudentShoudBeEditedWhenEditedStudent()
        {
            var studentId = new Guid("3b4e5908-7842-4523-9ea6-bb14befb3d0e");
            var newGroup = "Gp37";
            var studentStorage = new StudentStorage(DbContext);
            var student = DbContext.Students.FirstOrDefault(x => x.Id == studentId);
            var studentArgs = new StudentArgs()
            {
                Group = newGroup,
            };

            await studentStorage.EditStudent(studentId.ToString(), studentArgs);

            Assert.Equal(newGroup, DbContext.Students.FirstOrDefault(x => x.Id == studentId).Group);
        }

        [Fact]
        public async Task StudentShoudBeReturnedWhenGetedStudent()
        {
            var studentId = new Guid("3b4e5908-7842-4523-9ea6-bb14befb3d0e");
            var studentStorage = new StudentStorage(DbContext);
            var student = DbContext.Students.FirstOrDefault(x => x.Id == studentId);
            var newStudent = await studentStorage.GetStudent(studentId.ToString());

            Assert.Equal(student, newStudent);
        }

        [Fact]
        public async Task StudentShoudBeReturnedWhenGetedStudentByLogin()
        {
            var studentLogin = DbContext.Students.First().Login;
            var studentStorage = new StudentStorage(DbContext);
            var student = DbContext.Students.FirstOrDefault(x => x.Login == studentLogin);
            var newStudent = await studentStorage.GetStudent(studentLogin);

            Assert.Equal(student, newStudent);
        }
    }
}
