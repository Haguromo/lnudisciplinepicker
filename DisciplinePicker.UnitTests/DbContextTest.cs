﻿using System.Threading.Tasks;
using Xunit;

namespace DisciplinePicker.UnitTests
{
    public class DbContextTest : TestWithSqlite
    {
        [Fact]
        public async Task DatabaseIsAvailableAndCanBeConnectedTo()
        {
            Assert.True(await DbContext.Database.CanConnectAsync());
        }
    }
}
