﻿// <copyright file="CreateDisciplineViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    /// <summary>
    /// ViewModel of page for create discipline.
    /// </summary>
    public class CreateDisciplineViewModel
    {
        /// <summary>
        /// Gets or sets name of discipline.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description of discipline.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets department of discipline.
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// Gets or sets faculty of discipline.
        /// </summary>
        public string Faculty { get; set; }
    }
}
