﻿// <copyright file="CreateStudentViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    /// <summary>
    /// ViewModel of page for create student.
    /// </summary>
    public class CreateStudentViewModel
    {
        /// <summary>
        /// Gets or sets login of student.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Gets or sets password of student.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets name of student.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets group of student.
        /// </summary>
        public string Group { get; set; }

        /// <summary>
        /// Gets or sets cource of student.
        /// </summary>
        public int Course { get; set; }

        /// <summary>
        /// Gets or sets speciality of student.
        /// </summary>
        public string Speciality { get; set; }

        /// <summary>
        /// Gets or sets department of student.
        /// </summary>
        public string Department { get; set; }
    }
}
