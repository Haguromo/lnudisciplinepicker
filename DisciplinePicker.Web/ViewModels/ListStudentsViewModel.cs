﻿// <copyright file="ListStudentsViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System.Collections.Generic;
    using DisciplinePicker.Persistence.Model;
    using DisciplinePicker.Web.Models;
    using Microsoft.AspNetCore.Mvc.Rendering;

    /// <summary>
    /// ViewModel of page for represent list of students.
    /// </summary>
    public class ListStudentsViewModel
    {
        /// <summary>
        /// Gets or sets name of student.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets group of student.
        /// </summary>
        public string Group { get; set; }

        /// <summary>
        /// Gets or sets course of student.
        /// </summary>
        public int? Course { get; set; }

        /// <summary>
        /// Gets or sets peciality of student.
        /// </summary>
        public string Speciality { get; set; }

        /// <summary>
        /// Gets or sets department of student.
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// Gets or sets all students.
        /// </summary>
        public IEnumerable<Student> Students { get; set; }
    }
}
