﻿// <copyright file="AdminNotifyViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    /// <summary>
    /// Admin notification for students.
    /// </summary>
    public class AdminNotifyViewModel
    {
        /// <summary>
        /// Gets or sets text of notify.
        /// </summary>
        public string Notify { get; set; }
    }
}
