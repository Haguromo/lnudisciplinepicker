﻿// <copyright file="ListDisciplinesViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System.Collections.Generic;
    using DisciplinePicker.Persistence.Model;
    using DisciplinePicker.Web.Models;

    /// <summary>
    /// ViewModel of page for represent list of disciplines.
    /// </summary>
    public class ListDisciplinesViewModel
    {
        /// <summary>
        /// Gets or sets all disciplines.
        /// </summary>
        public IEnumerable<Discipline> Disciplines { get; set; }
    }
}
