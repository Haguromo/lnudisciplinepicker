﻿// <copyright file="RegisterViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// ViewModel of login page.
    /// </summary>
    public class RegisterViewModel
    {
        /// <summary>
        /// Gets or sets user name.
        /// </summary>
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets user login.
        /// </summary>
        [Required]
        [Display(Name = "Login")]
        public string Login { get; set; }

        /// <summary>
        /// Gets or sets user password.
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets confirmed user password.
        /// </summary>
        [Required]
        [Compare("Password", ErrorMessage = "password isn't equal")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        public string PasswordConfirm { get; set; }
    }
}
