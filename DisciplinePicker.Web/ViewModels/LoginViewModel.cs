﻿// <copyright file="LoginViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// ViewModel of login page.
    /// </summary>
    public class LoginViewModel
    {
        /// <summary>
        /// Gets or sets user login.
        /// </summary>
        [Required]
        [Display(Name = "Login")]
        public string Login { get; set; }

        /// <summary>
        /// Gets or sets user password.
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets remember the user.
        /// </summary>
        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        /// <summary>
        /// Gets or sets Url.
        /// </summary>
        public string ReturnUrl { get; set; }
    }
}
