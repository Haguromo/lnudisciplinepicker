﻿// <copyright file="PickerPageViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    /// <summary>
    /// Model for pagination.
    /// </summary>
    public class PickerPageViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PickerPageViewModel"/> class.
        /// Default constructor.
        /// </summary>
        /// <param name="page">Current page.</param>
        /// <param name="countPages">Count of all pages.</param>
        public PickerPageViewModel(int page, int countPages)
        {
            this.PageNumber = page;
            this.TotalPages = countPages;
        }

        /// <summary>
        /// Gets count of all pages.
        /// </summary>
        public int TotalPages { get; private set; }

        /// <summary>
        /// Gets current page number.
        /// </summary>
        public int PageNumber { get; private set; }
    }
}
