﻿// <copyright file="DisciplinePickerViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System.Collections.Generic;
    using DisciplinePicker.Persistence.Model;
    using DisciplinePicker.Web.Models;

    /// <summary>
    /// Enum represents sort order.
    /// </summary>
    public enum PickerSortOrder
    {
        /// <summary>
        /// Sort ascending by name.
        /// </summary>
        NameAsc,

        /// <summary>
        /// Sort descending by name.
        /// </summary>
        NameDesc,

        /// <summary>
        /// Sort ascending by faculty.
        /// </summary>
        FacultyAsc,

        /// <summary>
        /// Sort descending by faculty.
        /// </summary>
        FacultyDesc,

        /// <summary>
        /// Sort ascending by count registered on discipline.
        /// </summary>
        CountRegisteredAsc,

        /// <summary>
        /// Sort descending by count registered on discipline.
        /// </summary>
        CountRegisteredDesc,

        /// <summary>
        /// Sort ascending by count for a discipline.
        /// </summary>
        HoursAsc,

        /// <summary>
        /// Sort descending by count for a discipline.
        /// </summary>
        HoursDesc,

        /// <summary>
        /// Sort ascending by Lecturer name.
        /// </summary>
        LecturerAsc,

        /// <summary>
        /// Sort descending by Lecturer name.
        /// </summary>
        LecturerDesc,

        /// <summary>
        /// Sort ascending by registration status.
        /// </summary>
        RegisteredAsc,

        /// <summary>
        /// Sort descending by registration status.
        /// </summary>
        RegisteredDesc,
    }

    /// <summary>
    /// Main model that contains all other.
    /// </summary>
    public class DisciplinePickerViewModel
    {
        /// <summary>
        /// Gets or sets model for sorting.
        /// </summary>
        public PickerSortViewModel SortViewModel { get; set; }

        /// <summary>
        /// Gets or sets model for filtering.
        /// </summary>
        public PickerFilterViewModel FilterViewModel { get; set; }

        /// <summary>
        /// Gets or sets model for pagination.
        /// </summary>
        public PickerPageViewModel PageViewModel { get; set; }

        /// <summary>
        /// Gets or sets filtered discipline availabilities.
        /// </summary>
        public IEnumerable<DisciplineAvailability> Disciplines { get; set; }

        /// <summary>
        /// Gets or sets discipline availabilities on what user registered.
        /// </summary>
        public IEnumerable<DisciplineAvailability> RegisteredDisciplines { get; set; }
    }
}
