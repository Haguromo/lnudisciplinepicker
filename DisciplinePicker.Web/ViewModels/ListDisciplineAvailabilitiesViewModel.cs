﻿// <copyright file="ListDisciplineAvailabilitiesViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System.Collections.Generic;
    using DisciplinePicker.Persistence.Model;
    using DisciplinePicker.Web.Models;
    using Microsoft.AspNetCore.Mvc.Rendering;

    /// <summary>
    /// ViewModel of page for represent list of discipline availabilities.
    /// </summary>
    public class ListDisciplineAvailabilitiesViewModel
    {
        /// <summary>
        /// Gets or sets name of discipline availability.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets year of discipline availability.
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// Gets or sets semestr of discipline availability.
        /// </summary>
        public int? Semester { get; set; }

        /// <summary>
        /// Gets or sets lecturer of discipline availability.
        /// </summary>
        public string Lecturer { get; set; }

        /// <summary>
        /// Gets or sets all discipline availabilities.
        /// </summary>
        public IEnumerable<DisciplineAvailability> DisciplineAvailabilities { get; set; }
    }
}
