﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DisciplinePicker.Web.ViewModels
{
    public class StudentViewModel
    {
        /// <summary>
        /// Gets or sets universal id of student.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets name of student.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets password of student.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets login of student.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Gets or sets group of student.
        /// </summary>
        public string Group { get; set; }

        /// <summary>
        /// Gets or sets course of student.
        /// </summary>
        public int Course { get; set; }

        /// <summary>
        /// Gets or sets peciality of student.
        /// </summary>
        public string Speciality { get; set; }

        /// <summary>
        /// Gets or sets department of student.
        /// </summary>
        public string Department { get; set; }
    }
}
