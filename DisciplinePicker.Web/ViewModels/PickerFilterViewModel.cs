﻿// <copyright file="PickerFilterViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Mvc.Rendering;

    /// <summary>
    /// View model for filtering.
    /// </summary>
    public class PickerFilterViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PickerFilterViewModel"/> class.
        /// </summary>
        /// <param name="faculties">All faculties what was found.</param>
        /// <param name="selectedFaculty">Selected faculty for filtering.</param>
        /// <param name="query">Query for search.</param>
        /// <param name="semester">Semester for filtering.</param>
        public PickerFilterViewModel(List<string> faculties, string selectedFaculty, string query, int semester)
        {
            faculties.Insert(0, "Всі");
            this.Faculties = new SelectList(faculties, selectedFaculty);
            this.Semester = semester;
            this.Query = query;
            this.SelectedFaculty = selectedFaculty;
        }

        /// <summary>
        /// Gets query for search.
        /// </summary>
        public string Query { get; private set; }

        /// <summary>
        /// Gets faculties that was found.
        /// </summary>
        public SelectList Faculties { get; private set; }

        /// <summary>
        /// Gets faculty that was selected.
        /// </summary>
        public string SelectedFaculty { get; private set; }

        /// <summary>
        /// Gets semester for filtering.
        /// </summary>
        public int? Semester { get; private set; }
    }
}
