﻿// <copyright file="CreateLecturerViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    /// <summary>
    /// ViewModel of page for create lecturer.
    /// </summary>
    public class CreateLecturerViewModel
    {
        /// <summary>
        /// Gets or sets name of lecturer.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets department of lecturer.
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// Gets or sets faculty of lecturer.
        /// </summary>
        public string Faculty { get; set; }
    }
}
