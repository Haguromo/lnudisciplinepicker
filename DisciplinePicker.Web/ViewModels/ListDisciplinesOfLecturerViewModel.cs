﻿// <copyright file="ListDisciplinesOfLecturerViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System.Collections.Generic;
    using DisciplinePicker.Persistence.Model;
    using DisciplinePicker.Web.Models;

    /// <summary>
    /// Represents list discipline availabilities of lecturer.
    /// </summary>
    public class ListDisciplinesOfLecturerViewModel
    {
        /// <summary>
        /// Gets or sets name of lecturer.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets discipline availabilities of lecturer.
        /// </summary>
        public IEnumerable<DisciplineAvailability> DisciplineAvailabilities { get; set; }
    }
}
