﻿// <copyright file="CreateDisciplineAvailabilityViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System.Collections.Generic;
    using DisciplinePicker.Persistence.Model;
    using DisciplinePicker.Web.Models;

    /// <summary>
    /// ViewModel who represent page for creating discipline availability.
    /// </summary>
    public class CreateDisciplineAvailabilityViewModel
    {
        /// <summary>
        /// Gets or sets year of discipline availability.
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Gets or sets semester of discipline availability.
        /// </summary>
        public int Semester { get; set; }

        /// <summary>
        /// Gets or sets hours of discipline availability.
        /// </summary>
        public int Hours { get; set; }

        /// <summary>
        /// Gets or sets max count alloved students of discipline availability.
        /// </summary>
        public int MaxAllovedStudents { get; set; }

        /// <summary>
        /// Gets or sets year of discipline availability.
        /// </summary>
        public int MinAllovedStudents { get; set; }

        /// <summary>
        /// Gets or sets disciplineId of discipline availability.
        /// </summary>
        public string DisciplineId { get; set; }

        /// <summary>
        /// Gets or sets lecturerId of discipline availability.
        /// </summary>
        public string LecturerId { get; set; }

        /// <summary>
        /// Gets or sets discipline choises with subscribed students on discipline availability.
        /// </summary>
        public virtual ICollection<DisciplineChoise> DisciplineChoises { get; set; }

        /// <summary>
        /// Gets or sets all disciplines.
        /// </summary>
        public virtual ICollection<Discipline> Disciplines { get; set; }

        /// <summary>
        /// Gets or sets all lecturers.
        /// </summary>
        public virtual ICollection<Lecturer> Lecturers { get; set; }
    }
}
