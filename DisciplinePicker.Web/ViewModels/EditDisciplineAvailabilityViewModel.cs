﻿// <copyright file="EditDisciplineAvailabilityViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System;
    using System.Collections.Generic;
    using DisciplinePicker.Persistence.Model;
    using DisciplinePicker.Web.Models;

    /// <summary>
    /// ViewModel of page for edit discipline availability.
    /// </summary>
    public class EditDisciplineAvailabilityViewModel
    {
        /// <summary>
        /// Gets or sets universal identificator of discipline availability.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets year of discipline availability.
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Gets or sets semester of discipline availability.
        /// </summary>
        public int Semester { get; set; }

        /// <summary>
        /// Gets or sets hours of discipline availability.
        /// </summary>
        public int Hours { get; set; }

        /// <summary>
        /// Gets or sets max count alloved students of discipline availability.
        /// </summary>
        public int MaxAllovedStudents { get; set; }

        /// <summary>
        /// Gets or sets year of discipline availability.
        /// </summary>
        public int MinAllovedStudents { get; set; }

        /// <summary>
        /// Gets or sets lecturerId of discipline availability.
        /// </summary>
        public string LecturerId { get; set; }

        /// <summary>
        /// Gets or sets lecturerId of discipline availability.
        /// </summary>
        public string DisciplineId { get; set; }

        /// <summary>
        /// Gets or sets all disciplines.
        /// </summary>
        public virtual IEnumerable<Discipline> Disciplines { get; set; }

        /// <summary>
        /// Gets or sets all lecturers.
        /// </summary>
        public virtual IEnumerable<Lecturer> Lecturers { get; set; }
    }
}
