﻿// <copyright file="ListDisciplinesOfStudentViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System.Collections.Generic;
    using DisciplinePicker.Persistence.Model;
    using DisciplinePicker.Web.Models;
    using Microsoft.AspNetCore.Mvc.Rendering;

    /// <summary>
    /// Represents list discipline availabilities of student.
    /// </summary>
    public class ListDisciplinesOfStudentViewModel
    {
        /// <summary>
        /// Gets or sets name of student.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets group of student.
        /// </summary>
        public string Group { get; set; }

        /// <summary>
        /// Gets or sets discipline availabilities of student.
        /// </summary>
        public IEnumerable<DisciplineAvailability> DisciplineAvailabilities { get; set; }
    }
}
