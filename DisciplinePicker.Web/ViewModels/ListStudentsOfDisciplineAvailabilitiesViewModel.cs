﻿// <copyright file="ListStudentsOfDisciplineAvailabilitiesViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System.Collections.Generic;
    using DisciplinePicker.Persistence.Model;
    using DisciplinePicker.Web.Models;

    /// <summary>
    /// Represents list students of discipline availabilitiy.
    /// </summary>
    public class ListStudentsOfDisciplineAvailabilitiesViewModel
    {
        /// <summary>
        /// Gets or sets name of discipline.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets year of discipline availability.
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Gets or sets semester of discipline availability.
        /// </summary>
        public int Semestr { get; set; }

        /// <summary>
        /// Gets or sets students of discipline availability.
        /// </summary>
        public IEnumerable<Student> Students { get; set; }
    }
}
