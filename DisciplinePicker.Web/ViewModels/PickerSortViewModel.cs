﻿// <copyright file="PickerSortViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System;
    using System.Collections.Generic;
    using DisciplinePicker.Web.Models;
    using Microsoft.AspNetCore.Mvc.Rendering;

    /// <summary>
    /// Model for sorting.
    /// </summary>
    public class PickerSortViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PickerSortViewModel"/> class.
        /// </summary>
        /// <param name="order">Sorting order.</param>
        public PickerSortViewModel(PickerSortOrder order)
        {
            this.Order = order;
        }

        /// <summary>
        /// Gets Current sorting order.
        /// </summary>
        public PickerSortOrder Order { get; private set; }
    }
}
