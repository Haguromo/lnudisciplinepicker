﻿// <copyright file="ChangePasswordViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System;

    /// <summary>
    /// ViewModel for page of change password.
    /// </summary>
    public class ChangePasswordViewModel
    {
        /// <summary>
        /// Gets or sets universal identificator of student.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets login of student.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Gets or sets new password of student.
        /// </summary>
        public string NewPassword { get; set; }

        /// <summary>
        /// Gets or sets old password of student.
        /// </summary>
        public string OldPassword { get; set; }
    }
}
