﻿// <copyright file="EditAdminDataViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System;

    /// <summary>
    /// Represents view model for editing admin data.
    /// </summary>
    public class EditAdminDataViewModel
    {
        /// <summary>
        /// Gets or sets universal identificator of admin.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets password of admin.
        /// </summary>
        public string OldPassword { get; set; }

        /// <summary>
        /// Gets or sets password of admin.
        /// </summary>
        public string NewPassword { get; set; }

        /// <summary>
        /// Gets or sets login of admin.
        /// </summary>
        public string Login { get; set; }
    }
}
