﻿// <copyright file="ListLecturersViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System.Collections.Generic;
    using DisciplinePicker.Persistence.Model;
    using DisciplinePicker.Web.Models;

    /// <summary>
    /// ViewModel of page for represent list of lecturers.
    /// </summary>
    public class ListLecturersViewModel
    {
        /// <summary>
        /// Gets or sets all lecturers.
        /// </summary>
        public IEnumerable<Lecturer> Lecturers { get; set; }
    }
}
