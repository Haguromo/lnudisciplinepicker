﻿// <copyright file="EditLecturerViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System;

    /// <summary>
    /// ViewModel of page for edit lecturer.
    /// </summary>
    public class EditLecturerViewModel
    {
        /// <summary>
        /// Gets or sets universal identificator of lecturer.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets name of lecturer.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets department of lecturer.
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// Gets or sets faculty of lecturer.
        /// </summary>
        public string Faculty { get; set; }
    }
}
