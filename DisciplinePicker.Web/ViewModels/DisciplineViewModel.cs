﻿using DisciplinePicker.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DisciplinePicker.Web.ViewModels
{
    public class DisciplineViewModel
    {
        /// <summary>
        /// Gets or sets universal id of discipline.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets name of discipline.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description of discipline.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets description of discipline.
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// Gets or sets description of discipline.
        /// </summary>
        public string Faculty { get; set; }

        public ICollection<DisciplineAvailability> DisciplineAvailabilities { get; set; }
    }
}
