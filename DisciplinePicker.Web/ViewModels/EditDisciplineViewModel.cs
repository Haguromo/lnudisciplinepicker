﻿// <copyright file="EditDisciplineViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.ViewModels
{
    using System;

    /// <summary>
    /// ViewModel of page for edit discipline.
    /// </summary>
    public class EditDisciplineViewModel
    {
        /// <summary>
        /// Gets or sets universal identificator of discipline.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets name of discipline.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description of discipline.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets department of discipline.
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// Gets or sets faculty of discipline.
        /// </summary>
        public string Faculty { get; set; }
    }
}
