﻿// <copyright file="FileLoggerExtensions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.Models
{
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Represent class for manage of logger extentions.
    /// </summary>
    public static class FileLoggerExtensions
    {
        /// <summary>
        /// Add file logger in the project.
        /// </summary>
        /// <param name="factory">Logger factory.</param>
        /// <param name="filePath">Path to logger file.</param>
        /// <returns>Logger factory with added provider.</returns>
        public static ILoggerFactory AddFile(this ILoggerFactory factory, string filePath)
        {
            factory.AddProvider(new FileLoggerProvider(filePath));
            return factory;
        }
    }
}
