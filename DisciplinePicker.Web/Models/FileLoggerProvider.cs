﻿// <copyright file="FileLoggerProvider.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.Models
{
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Provider for logger file.
    /// </summary>
    public class FileLoggerProvider : ILoggerProvider
    {
        private readonly string path;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileLoggerProvider"/> class.
        /// </summary>
        /// <param name="path">Path to logger file.</param>
        public FileLoggerProvider(string path)
        {
            this.path = path;
        }

        /// <inheritdoc/>
        public ILogger CreateLogger(string categoryName)
        {
            return new FileLogger(this.path);
        }

        /// <inheritdoc/>
        public void Dispose()
        {
        }
    }
}
