﻿// <copyright file="FileLogger.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.Models
{
    using System;
    using System.IO;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Represent class for manage of logger.
    /// </summary>
    public class FileLogger : ILogger
    {
        private static readonly object Lock = new object();
        private readonly string filePath;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileLogger"/> class.
        /// </summary>
        /// <param name="path">Path to logger file.</param>
        public FileLogger(string path)
        {
            this.filePath = path;
        }

        /// <inheritdoc/>
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        /// <inheritdoc/>
        public bool IsEnabled(LogLevel logLevel)
        {
            // return logLevel == LogLevel.Trace;
            return true;
        }

        /// <inheritdoc/>
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (formatter != null)
            {
                lock (Lock)
                {
                    File.AppendAllText(this.filePath, formatter(state, exception) + Environment.NewLine);
                }
            }
        }
    }
}
