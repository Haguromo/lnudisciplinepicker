﻿function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    document.getElementById("buttonMenu").style.opacity = 0;
}

function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    buttonOpacity();
}

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function buttonOpacity() {
    await timeout(450);
    document.getElementById("buttonMenu").style.opacity = 1;
}

function disciplineDataWindowModel(name, description, department, faculty, minAllowedStudents, maxAllowedStudents) {
    document.getElementById("Name").innerHTML = name;
    document.getElementById("Description").innerHTML = description;
    document.getElementById("Department").innerHTML = department;
    document.getElementById("Faculty").innerHTML = faculty;
    document.getElementById("MinAllowedStudents").innerHTML = minAllowedStudents;
    document.getElementById("MaxAllowedStudents").innerHTML = maxAllowedStudents;
}

function disciplineDescriptionWindowModel(name, description) {
    document.getElementById("Name").innerHTML = name;
    document.getElementById("Description").innerHTML = description;
}

