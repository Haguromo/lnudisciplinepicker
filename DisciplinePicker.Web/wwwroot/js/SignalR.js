﻿import { options, info } from 'toastr'

export function displayInfo(message) {
    options.timeOut = 2000;
    options.positionClass = 'toast-bottom-right';

    info(message);
}