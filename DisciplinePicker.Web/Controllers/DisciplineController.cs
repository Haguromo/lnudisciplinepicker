﻿// <copyright file="DisciplineController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DisciplinePicker.Web.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using DisciplinePicker.Persistence.Infrastructure;
    using DisciplinePicker.Persistence.Model;
    using DisciplinePicker.Web.Models;
    using DisciplinePicker.Web.ViewModels;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Controller to manage discipline registration process.
    /// </summary>
    [Authorize]
    public class DisciplineController : Controller
    {
        private readonly DisciplinePickerDatabaseContext db;
        private readonly ILogger<DisciplineController> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="DisciplineController"/> class.
        /// </summary>
        /// <param name="context">Application db context.</param>
        /// <param name="logger">Logger.</param>
        public DisciplineController(DisciplinePickerDatabaseContext context, ILogger<DisciplineController> logger)
        {
            this.db = context;
            this.logger = logger;
        }

        /// <summary>
        /// Actione to view disciplines to registrate.
        /// </summary>
        /// <param name="query">Query for search among disciplines.</param>
        /// <param name="faculty">Faculty for filter.</param>
        /// /// <param name="page">Current page.</param>
        /// <param name="sortOrder">Sort order.</param>
        /// <param name="semester">Semester for filter.</param>
        /// <returns>Filtered sorted disciplines.</returns>
        [Route("[controller]/[action]/{semester:int?}")]
        public async Task<IActionResult> ViewCatalog(
            string query,
            string faculty,
            int page = 1,
            PickerSortOrder sortOrder = PickerSortOrder.NameAsc,
            [FromRoute]int semester = 1)
        {
            var availabilities = this.db.DisciplineAvailabilities.Include("Discipline").Include("DisciplineChoises").Include("Lecturer");

            var currentUser = this.db.Students.FirstOrDefault(x => x.Login == this.HttpContext.User.Identity.Name);

            var currentYear = DateTime.Now.Year;
            availabilities = availabilities.Where(x => x.Semester % 2 == semester % 2);
            if (semester == 1)
            {
                availabilities = availabilities.Where(x => x.Year == currentYear);
            }
            else
            {
                availabilities = availabilities.Where(x => x.Year == currentYear + 1);
            }

            availabilities = availabilities.Where(x => x.MaxAllovedStudents > x.DisciplineChoises.Count);

            if (!string.IsNullOrWhiteSpace(query))
            {
                availabilities = availabilities.Where(x => x.Discipline.Name.Contains(query) || x.Lecturer.Name.Contains(query));
            }

            if (!string.IsNullOrWhiteSpace(faculty) && !string.Equals(faculty, "Всі"))
            {
                availabilities = availabilities.Where(x => x.Discipline.Faculty.ToLower() == faculty.ToLower());
            }

            switch (sortOrder)
            {
                case PickerSortOrder.FacultyAsc:
                    availabilities = availabilities.OrderBy(x => x.Discipline.Faculty);
                    break;
                case PickerSortOrder.FacultyDesc:
                    availabilities = availabilities.OrderByDescending(x => x.Discipline.Faculty);
                    break;
                case PickerSortOrder.CountRegisteredAsc:
                    availabilities = availabilities.OrderBy(x => x.DisciplineChoises.Count);
                    break;
                case PickerSortOrder.CountRegisteredDesc:
                    availabilities = availabilities.OrderByDescending(x => x.DisciplineChoises.Count);
                    break;
                case PickerSortOrder.HoursAsc:
                    availabilities = availabilities.OrderBy(x => x.Hours);
                    break;
                case PickerSortOrder.HoursDesc:
                    availabilities = availabilities.OrderByDescending(x => x.Hours);
                    break;
                case PickerSortOrder.LecturerAsc:
                    availabilities = availabilities.OrderBy(x => x.Lecturer.Name);
                    break;
                case PickerSortOrder.LecturerDesc:
                    availabilities = availabilities.OrderByDescending(x => x.Lecturer.Name);
                    break;
                case PickerSortOrder.NameDesc:
                    availabilities = availabilities.OrderByDescending(x => x.Lecturer.Name);
                    break;
                case PickerSortOrder.RegisteredAsc:
                    availabilities = availabilities.OrderByDescending(x => x.DisciplineChoises.Count(y => y.StudentId == currentUser.Id));
                    break;
                case PickerSortOrder.RegisteredDesc:
                    availabilities = availabilities.OrderBy(x => x.DisciplineChoises.Count(y => y.StudentId == currentUser.Id));
                    break;
                case PickerSortOrder.NameAsc:
                default:
                    availabilities = availabilities.OrderBy(x => x.Discipline.Name);
                    break;
            }

            var faculties = await this.db.Disciplines.Select(x => x.Faculty).Distinct().ToListAsync();

            int countPages = (int)Math.Ceiling((double)(await availabilities.CountAsync() / 10)) + 1;

            availabilities = availabilities.Skip((page - 1) * 10).Take(10);

            var registered = availabilities
                .Where(x => x.DisciplineChoises.FirstOrDefault(y => y.StudentId == currentUser.Id) != null);

            var model = new DisciplinePickerViewModel()
            {
                SortViewModel = new PickerSortViewModel(sortOrder),
                FilterViewModel = new PickerFilterViewModel(faculties, faculty, query, semester),
                PageViewModel = new PickerPageViewModel(page, countPages),
                Disciplines = availabilities,
                RegisteredDisciplines = registered,
            };

            this.logger
                .LogInformation(
                "Output values: count faculties-{0}, count availabilities match filters-{1}",
                faculties.Count(),
                availabilities.Count());

            return this.View("Catalog", model);
        }

        /// <summary>
        /// Registrate on discipline.
        /// </summary>
        /// <param name="id">Id of registration availability.</param>
        /// <param name="returnUrl">Url of previous catalog view.</param>
        /// <returns>Redirects to previous catalog view.</returns>
        public async Task<IActionResult> ChooseDiscipline(Guid id, string returnUrl)
        {
            this.logger.LogInformation("Return url: {0}", returnUrl);
            var currentUser = await this.db.Students.FirstOrDefaultAsync(x => x.Login == this.HttpContext.User.Identity.Name);
            var choosenAvailability = await this.db
                .DisciplineAvailabilities
                .FirstOrDefaultAsync(x => x.Id == id);
            if (choosenAvailability != null)
            {
                this.db.DisciplineChoises.Add(new DisciplineChoise()
                {
                    DisciplineAvailability = choosenAvailability,
                    Student = currentUser,
                });
                await this.db.SaveChangesAsync();
                this.logger.LogInformation("Added registration choice: id={0}, studentid={1}", id, currentUser.Id);
            }
            else
            {
                this.logger.LogInformation("Register on availability twice: id={0}, studentid={1}", id, currentUser.Id);
            }

            return this.Redirect(returnUrl);
        }

        /// <summary>
        /// Cancel registration on discipline.
        /// </summary>
        /// <param name="id">Id of registration availability.</param>
        /// <param name="returnUrl">Url of previous catalog view.</param>
        /// <returns>Redirects to previous catalog view.</returns>
        public async Task<IActionResult> CancelChoice(Guid id, string returnUrl)
        {
            this.logger.LogInformation("Return url: {0}", returnUrl);
            var currentUser = await this.db.Students.FirstOrDefaultAsync(x => x.Login == this.HttpContext.User.Identity.Name);
            var choosenAvailability = await this.db
                .DisciplineAvailabilities
                .FirstOrDefaultAsync(x => x.Id == id);
            if (choosenAvailability != null)
            {
                var aval = await this.db.DisciplineChoises
                    .FirstOrDefaultAsync(x => x.DisciplineAvailabilityId == choosenAvailability.Id && x.StudentId == currentUser.Id);
                this.db.DisciplineChoises.Remove(aval);
                await this.db.SaveChangesAsync();
                this.logger.LogInformation("Found availability to register: id={0}, studentid={1}", id, currentUser.Id);
            }
            else
            {
                this.logger.LogInformation("Not found availability to register: id={0}, studentid={1}", id, currentUser.Id);
            }

            return this.Redirect(returnUrl);
        }
    }
}
