﻿// <copyright file="AccountController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using DisciplinePicker.Persistence.Infrastructure;
using DisciplinePicker.Persistence.Model;
using DisciplinePicker.Web.ViewModels;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DisciplinePicker.Web.Controllers
{
    /// <summary>
    /// Controller for login, logout and registration.
    /// </summary>
    public class AccountController : Controller
    {
        // Database cont ext.
        private readonly DisciplinePickerDatabaseContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// </summary>
        /// <param name="context">Database context.</param>
        public AccountController(DisciplinePickerDatabaseContext context)
        {
            this.db = context;
        }

        /// <summary>
        /// Get method of registration.
        /// </summary>
        /// <returns>The view of registration.</returns>
        [HttpGet]
        public IActionResult Register() => this.View();

        /// <summary>
        /// Post method of registration.
        /// </summary>
        /// <param name="model">The ViewModel of regitrstion view.</param>
        /// <returns>The view of authorized or wrong input data or this view again.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                Student student = await this.db.Students.FirstOrDefaultAsync(u => u.Login == model.Login);
                if (student == null)
                {
                    student = new Student { Login = model.Login, Password = model.Password };
                    Role studentRole = await this.db.Roles.FirstOrDefaultAsync(r => r.Name == "student");
                    if (studentRole != null)
                    {
                        student.Role = studentRole;
                    }

                    this.db.Students.Add(student);
                    await this.db.SaveChangesAsync();
                    await this.Authenticate(student);
                    return this.RedirectToAction("Index", "Home");
                }
                else
                {
                    this.ModelState.AddModelError(string.Empty, "Wrong login or password!");
                }
            }

            return this.View(model);
        }

        /// <summary>
        /// Get method of registration.
        /// </summary>
        /// <param name="returnUrl">The returned url.</param>
        /// <returns>The view of registration.</returns>
        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            if ((bool)this.User?.Identity?.IsAuthenticated)
            {
                return this.RedirectToAction("Index");
            }

            return this.View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        /// <summary>
        /// Post method of login.
        /// </summary>
        /// <param name="model">The ViewModel of login view.</param>
        /// <returns>The view of authorized or wrong input data or this view again.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                Student user = await this.db.Students.Include(x => x.Role).FirstOrDefaultAsync(u => u.Login == model.Login && u.Password == model.Password);
                if (user != null)
                {
                    await this.Authenticate(user);
                    if (user.Role.Name == "admin")
                    {
                        return this.RedirectToAction("ListDisciplineAvailabilities", "Admin");
                    }

                    if (!string.IsNullOrWhiteSpace(model.ReturnUrl))
                    {
                        return this.Redirect(model.ReturnUrl);
                    }
                    else
                    {
                        return this.RedirectToAction("Index");
                    }
                }

                this.ModelState.AddModelError(string.Empty, "Wrong login or password!");
            }

            return this.View(model);
        }

        /// <summary>
        /// Main page with student info.
        /// </summary>
        /// <returns>Student model.</returns>
        [Authorize]
        public IActionResult Index()
        {
            var student = this.db.Students.Include(x => x.Role).FirstOrDefault(u => u.Login == this.User.Identity.Name);
            var studentViewModel = new StudentViewModel
            {
                Course = student.Course,
                Department = student.Department,
                Group = student.Group,
                Id = student.Id,
                Login = student.Login,
                Password = student.Password,
                Name = student.Name,
                Speciality = student.Speciality,
            };

            var availabilities = this.db.DisciplineAvailabilities.Where(x => x.Year >= DateTime.Now.Year);
            this.ViewData["IsAvailable"] = availabilities.Count() != 0;

            if (student.Role.Name == "admin")
            {
                return this.RedirectToAction("ListDisciplineAvailabilities", "Admin");
            }

            return this.View("Index", studentViewModel);
        }

        /// <summary>
        /// Post method of logout.
        /// </summary>
        /// <returns>Start view.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await this.HttpContext.SignOutAsync();
            return this.RedirectToAction("Index");
        }

        private async Task Authenticate(Student student)
        {
            var claims = new List<Claim>
                    {
                        new Claim(ClaimsIdentity.DefaultNameClaimType, student.Login),
                        new Claim(ClaimsIdentity.DefaultRoleClaimType, student.Role?.Name),
                    };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await this.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
    }
}
