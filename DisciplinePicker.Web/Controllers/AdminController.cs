﻿// <copyright file="AdminController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using DisciplinePicker.Persistence.Infrastructure;
using DisciplinePicker.Persistence.Infrastructure.Repository;
using DisciplinePicker.Persistence.Model;
using DisciplinePicker.Web.SignalR;
using DisciplinePicker.Web.ViewModels;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DisciplinePicker.Web.Controllers
{
    /// <summary>
    /// Controller for admile role.
    /// </summary>
    public class AdminController : Controller
    {
        private readonly IStudentStorage _studentStorage;
        private readonly ILecturerStorage _lecturerStorage;
        private readonly IDisciplineStorage _disciplineStorage;
        private readonly IDisciplineAvailabilityStorage _disciplineAvailabilityStorage;
        private readonly IDisciplineChoiseStorage _disciplineChoiseStorage;
        private readonly IAdminStorage _adminStorage;

        private readonly IHubContext<NotifyHub> _hubContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminController"/> class.
        /// </summary>
        /// <param name="context">DataBase context.</param>
        public AdminController(IHubContext<NotifyHub> hubContext, IAdminStorage adminStorage, IStudentStorage studentStorage, ILecturerStorage lecturerStorage, IDisciplineStorage disciplineStorage, IDisciplineAvailabilityStorage disciplineAvailabilityStorage, IDisciplineChoiseStorage disciplineChoiseStorage)
        {
            _studentStorage = studentStorage;
            _disciplineAvailabilityStorage = disciplineAvailabilityStorage;
            _disciplineChoiseStorage = disciplineChoiseStorage;
            _disciplineStorage = disciplineStorage;
            _lecturerStorage = lecturerStorage;
            _adminStorage = adminStorage;

            _hubContext = hubContext;
        }

        /// <summary>
        /// Get method notification for students.
        /// </summary>
        /// <returns>View AdminNotify.</returns>
        public IActionResult AdminNotify()
        {
            return this.View();
        }

        /// <summary>
        /// Post method notification for students.
        /// </summary>
        /// <param name="model">NotifyArgs.</param>
        /// <returns>View ListDisciplineAvailabilities.</returns>
        [HttpPost]
        public IActionResult AdminNotify(AdminNotifyViewModel model)
        {
            _hubContext.Clients.All.SendAsync("Send", model.Notify);

            return this.RedirectToAction("ListDisciplineAvailabilities");
        }

        /// <summary>
        /// Get method for view of changing student password.
        /// </summary>
        /// <param name="id">Id of student.</param>
        /// <returns>The view of changing password or 404.</returns>
        public async Task<IActionResult> ChangePasswordStudent(string id)
        {
            var student = await _studentStorage.GetStudent(id);
            if (student == null)
            {
                return this.NotFound();
            }

            ChangePasswordViewModel model = new ChangePasswordViewModel { Id = student.Id, Login = student.Login, OldPassword = student.Password };
            return this.View(model);
        }

        /// <summary>
        /// Post method for view of changing student password.
        /// </summary>
        /// <param name="model">ViewModel of view ChangePassword.</param>
        /// <returns>The view of ListStudent or this page again if query is not valid.</returns>
        [HttpPost]
        public async Task<IActionResult> ChangePasswordStudent(ChangePasswordViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                await _studentStorage.ChangeStudentsPassword(model.Id.ToString(), model.NewPassword);
                return this.RedirectToAction("ListStudents");
            }

            return this.View(model);
        }

        /// <summary>
        /// Get method for view of creating discipline.
        /// </summary>
        /// <returns>The view of creating discipline.</returns>
        public IActionResult CreateDiscipline() => this.View();

        /// <summary>
        /// Post method for view of creating discipline.
        /// </summary>
        /// <param name="model">ViewModel of view CreateDiscipline.</param>
        /// <returns>The view of ListDisciplines or this page again if query is not valid.</returns>
        [HttpPost]
        public async Task<IActionResult> CreateDiscipline(CreateDisciplineViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var discipline = await _disciplineStorage.GetDisciplineByName(model.Name);
                if (discipline == null)
                {
                    await _disciplineStorage.AddDiscipline(new DisciplineArgs { Department = model.Department, Description = model.Description, Name = model.Name, Faculty = model.Faculty });
                    return this.RedirectToAction("ListDisciplines");
                }
                else
                {
                    // Error. The discipline with same name is already exists.
                }
            }

            return this.View(model);
        }

        /// <summary>
        /// Get method for view of creating discipline availability.
        /// </summary>
        /// <returns>The view of creating discipline availability.</returns>
        public async Task<IActionResult> CreateDisciplineAvailability()
        {
            return View(new CreateDisciplineAvailabilityViewModel() { Disciplines = await _disciplineStorage.GetAllDisciplines(), Lecturers = await _lecturerStorage.GetAllLecturers(), });
        }

        /// <summary>
        /// Post method for view of creating discipline availability.
        /// </summary>
        /// <param name="model">ViewModel of view CreateDisciplineAvailability.</param>
        /// <returns>The view of ListDisciplineAvailability or this page again if query is not valid.</returns>
        [HttpPost]
        public async Task<IActionResult> CreateDisciplineAvailability(CreateDisciplineAvailabilityViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var availabilityArgs = new AvailabilityArgs
                {
                    DisciplineId = model.DisciplineId,
                    LecturerId = model.LecturerId,
                    Hours = model.Hours,
                    MaxAllovedStudents = model.MaxAllovedStudents,
                    MinAllovedStudents = model.MinAllovedStudents,
                    Semester = model.Semester,
                    Year = model.Year,
                };
                await _disciplineAvailabilityStorage.AddNewAvailability(availabilityArgs);
                return this.RedirectToAction("ListDisciplineAvailabilities");
            }

            return this.View(model);
        }

        /// <summary>
        /// Get method for view of creating lecturer.
        /// </summary>
        /// <returns>The view of creating lecturer.</returns>
        public IActionResult CreateLecturer() => this.View();

        /// <summary>
        /// Post method for view of creating lecturer.
        /// </summary>
        /// <param name="model">ViewModel of view CreateLecturer.</param>
        /// <returns>The view of ListLecturers or this page again if query is not valid.</returns>
        [HttpPost]
        public async Task<IActionResult> CreateLecturer(CreateLecturerViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var lectorer = await _lecturerStorage.GetLecturerByName(model.Name);
                if (lectorer == null)
                {
                    await _lecturerStorage.AddLecturer(new NewLecturerArgs { Name = model.Name, Department = model.Department, Faculty = model.Faculty });
                    return this.RedirectToAction("ListLecturers");
                }
                else
                {
                    // Error. The lecturer with same name is already exists.
                }
            }

            return this.View(model);
        }

        /// <summary>
        /// Get method for view of creating student.
        /// </summary>
        /// <returns>The view of creating student.</returns>
        public IActionResult CreateStudent() => this.View();

        /// <summary>
        /// Post method for view of creating student.
        /// </summary>
        /// <param name="model">ViewModel of view CreateStudent.</param>
        /// <returns>The view of ListStudents or this page again if query is not valid.</returns>
        [HttpPost]
        public async Task<IActionResult> CreateStudent(CreateStudentViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = await _studentStorage.GetStudentByLogin(model.Login);
                if (user == null)
                {
                    var studentCreateArgs = new StudentArgs
                    {
                        Course = model.Course,
                        Department = model.Department,
                        Group = model.Group,
                        Login = model.Login,
                        Name = model.Name,
                        Password = model.Password,
                        Speciality = model.Speciality,
                    };
                    await _studentStorage.AddNewStudent(studentCreateArgs);
                    return this.RedirectToAction("ListStudents");
                }
                else
                {
                    // Error. The student with same login is already exists.
                }
            }

            return this.View(model);
        }

        /// <summary>
        /// Post Method for deleting discipline.
        /// </summary>
        /// <param name="id">Id of discipline.</param>
        /// <returns>The view of ListDisciplines.</returns>
        [HttpPost]
        public async Task<IActionResult> DeleteDiscipline(string id)
        {
            var discipline = await _disciplineStorage.GetDiscipline(id);
            if (discipline != null)
            {
                await _disciplineStorage.DeleteDiscipline(id);
            }

            return this.RedirectToAction("ListDisciplines");
        }

        /// <summary>
        /// Post Method for deleting discipline availability.
        /// </summary>
        /// <param name="id">Id of discipline availability.</param>
        /// <returns>The view of ListDisciplineAvailabilities.</returns>
        [HttpPost]
        public async Task<IActionResult> DeleteDisciplineAvailability(string id)
        {
            var disciplineAvailability = await _disciplineAvailabilityStorage.GetAvailability(id);

            if (disciplineAvailability != null)
            {
                await _disciplineAvailabilityStorage.DeleteAvailability(id);
            }

            return this.RedirectToAction("ListDisciplineAvailabilities");
        }

        /// <summary>
        /// Post Method for deleting lecturer.
        /// </summary>
        /// <param name="id">Id of lecturer.</param>
        /// <returns>The view of ListLecturers.</returns>
        [HttpPost]
        public async Task<IActionResult> DeleteLecturer(string id)
        {
            var lecturer = await _lecturerStorage.GetLecturer(id);
            if (lecturer != null)
            {
                await _lecturerStorage.DeleteLecturer(id);
            }

            return this.RedirectToAction("ListLecturers");
        }

        /// <summary>
        /// Post Method for deleting student.
        /// </summary>
        /// <param name="id">Id of student.</param>
        /// <returns>The view of ListStudents.</returns>
        [HttpPost]
        public async Task<IActionResult> DeleteStudent(string id)
        {
            var user = await _studentStorage.GetStudent(id);
            if (user != null)
            {
                await _studentStorage.DeleteStudent(id);
            }

            return this.RedirectToAction("ListStudents");
        }

        /// <summary>
        /// Get method for view of editing admin.
        /// </summary>
        /// <returns>The view of editing admin data.</returns>
        public async Task<IActionResult> EditAdminData()
        {
            var admin = await _adminStorage.GetAdmin();
            if (admin == null)
            {
                return this.NotFound();
            }

            EditAdminDataViewModel model = new EditAdminDataViewModel
            {
                Id = admin.Id,
                Login = admin.Login,
                OldPassword = admin.Password,
            };
            return this.View(model);
        }

        /// <summary>
        /// Post method for view of editing discipline.
        /// </summary>
        /// <param name="model">ViewModel of view EditDiscipline.</param>
        /// <returns>The view of ListDisciplines or this page again if query is not valid.</returns>
        [HttpPost]
        public async Task<IActionResult> EditAdminData(EditAdminDataViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var admin = await _adminStorage.GetAdmin();
                if (admin != null)
                {
                    await _adminStorage.EditAdminData(login: model.Login, password: model.NewPassword);
                    return this.RedirectToAction("ListDisciplineAvailabilities");
                }
            }

            return this.View(model);
        }

        /// <summary>
        /// Get method for view of editing discipline.
        /// </summary>
        /// <param name="id">Id of discipline.</param>
        /// <returns>The view of creating discipline.</returns>
        public async Task<IActionResult> EditDiscipline(string id)
        {
            var discipline = await _disciplineStorage.GetDiscipline(id);
            if (discipline == null)
            {
                return this.NotFound();
            }

            EditDisciplineViewModel model = new EditDisciplineViewModel
            {
                Id = discipline.Id, Name = discipline.Name, Description = discipline.Description, Department = discipline.Department, Faculty = discipline.Faculty,
            };
            return this.View(model);
        }

        /// <summary>
        /// Post method for view of editing discipline.
        /// </summary>
        /// <param name="model">ViewModel of view EditDiscipline.</param>
        /// <returns>The view of ListDisciplines or this page again if query is not valid.</returns>
        [HttpPost]
        public async Task<IActionResult> EditDiscipline(EditDisciplineViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var discipline = await _disciplineStorage.GetDiscipline(model.Id.ToString());
                if (discipline != null)
                {
                    var disciplineEditArgs = new DisciplineArgs
                    {
                        Department = model.Department,
                        Description = model.Description,
                        Faculty = model.Faculty,
                        Name = model.Name,
                    };

                    await _disciplineStorage.EditDiscipline(model.Id.ToString(), disciplineEditArgs);
                    return this.RedirectToAction("ListDisciplines");
                }
            }

            return this.View(model);
        }

        /// <summary>
        /// Get method for view of editing discipline availability.
        /// </summary>
        /// <param name="id">Id of discipline availability.</param>
        /// <returns>The view of creating discipline availability.</returns>
        public async Task<IActionResult> EditDisciplineAvailability(string id)
        {
            var disciplineAvailability = await _disciplineAvailabilityStorage.GetAvailability(id);
            if (disciplineAvailability == null)
            {
                return this.NotFound();
            }

            var disciplines = await _disciplineStorage.GetAllDisciplines();
            var lecturers = await _lecturerStorage.GetAllLecturers();

            EditDisciplineAvailabilityViewModel model = new EditDisciplineAvailabilityViewModel
            {
                Id = disciplineAvailability.Id,
                Hours = disciplineAvailability.Hours,
                MaxAllovedStudents = disciplineAvailability.MaxAllovedStudents,
                MinAllovedStudents = disciplineAvailability.MinAllovedStudents,
                Semester = disciplineAvailability.Semester,
                Year = disciplineAvailability.Year,
                DisciplineId = disciplineAvailability.DisciplineId.ToString(),
                LecturerId = disciplineAvailability.LecturerId.ToString(),
                Disciplines = disciplines,
                Lecturers = lecturers,
            };
            return this.View(model);
        }

        /// <summary>
        /// Post method for view of editing discipline availability.
        /// </summary>
        /// <param name="model">ViewModel of view EditDisciplineAvailability.</param>
        /// <returns>The view of ListDisciplineAvailabilities or this page again if query is not valid.</returns>
        [HttpPost]
        public async Task<IActionResult> EditDisciplineAvailability(EditDisciplineAvailabilityViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var disciplineAvailability = _disciplineAvailabilityStorage.GetAvailability(model.Id.ToString());
                if (disciplineAvailability != null)
                {
                    var disciplineAvailabilityEditArgs = new AvailabilityArgs
                    {
                        DisciplineId = model.DisciplineId,
                        LecturerId = model.LecturerId,
                        Hours = model.Hours,
                        MaxAllovedStudents = model.MaxAllovedStudents,
                        MinAllovedStudents = model.MinAllovedStudents,
                        Semester = model.Semester,
                        Year = model.Year,
                    };

                    await _disciplineAvailabilityStorage.EditAvailability(model.Id.ToString(), disciplineAvailabilityEditArgs);
                    return this.RedirectToAction("ListDisciplineAvailabilities");
                }
            }

            return this.View(model);
        }

        /// <summary>
        /// Get method for view of editing lecturer.
        /// </summary>
        /// <param name="id">Id of lecturer.</param>
        /// <returns>The view of creating lecturer.</returns>
        public async Task<IActionResult> EditLecturer(string id)
        {
            var lecturer = await _lecturerStorage.GetLecturer(id);
            if (lecturer == null)
            {
                return this.NotFound();
            }

            EditLecturerViewModel model = new EditLecturerViewModel
            {
                Id = lecturer.Id,
                Name = lecturer.Name,
                Department = lecturer.Department,
                Faculty = lecturer.Faculty,
            };
            return this.View(model);
        }

        /// <summary>
        /// Post method for view of editing lecturer.
        /// </summary>
        /// <param name="model">ViewModel of view EditLecturer.</param>
        /// <returns>The view of ListLecturers or this page again if query is not valid.</returns>
        [HttpPost]
        public async Task<IActionResult> EditLecturer(EditLecturerViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var lecturer = await _lecturerStorage.GetLecturer(model.Id.ToString());
                if (lecturer != null)
                {
                    var editLecturerArgs = new NewLecturerArgs
                    {
                        Department = model.Department,
                        Faculty = model.Faculty,
                        Name = model.Name,
                    };

                    await _lecturerStorage.EditLecturer(model.Id.ToString(), editLecturerArgs);
                    return this.RedirectToAction("ListLecturers");
                }
            }

            return this.View(model);
        }

        /// <summary>
        /// Get method for view of editing student.
        /// </summary>
        /// <param name="id">Id of student.</param>
        /// <returns>The view of creating student.</returns>
        public async Task<IActionResult> EditStudent(string id)
        {
            var student = await _studentStorage.GetStudent(id);
            if (student == null)
            {
                return this.NotFound();
            }

            EditStudentViewModel model = new EditStudentViewModel
            {
                Id = student.Id, Login = student.Login, Course = student.Course, Department = student.Department,
                Group = student.Group, Name = student.Name, Password = student.Password, Speciality = student.Speciality,
            };
            return this.View(model);
        }

        /// <summary>
        /// Post method for view of editing student.
        /// </summary>
        /// <param name="model">ViewModel of view EditStudent.</param>
        /// <returns>The view of ListStudents or this page again if query is not valid.</returns>
        [HttpPost]
        public async Task<IActionResult> EditStudent(EditStudentViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var student = await _studentStorage.GetStudent(model.Id.ToString());
                if (student != null)
                {
                    var studentEditArgs = new StudentArgs
                    {
                        Course = model.Course,
                        Department = model.Department,
                        Group = model.Group,
                        Login = model.Login,
                        Name = model.Name,
                        Password = model.Password,
                        Speciality = model.Speciality,
                    };
                    await _studentStorage.EditStudent(model.Id.ToString(), studentEditArgs);
                    return this.RedirectToAction("ListStudents");
                }
            }

            return this.View(model);
        }

        /// <summary>
        /// Get method for view with list of disciplines.
        /// </summary>
        /// <param name="name">Name of discipline for filtring.</param>
        /// <param name="faculty">Faculty of discipline for filtring.</param>
        /// <param name="department">Department of discipline for filtring.</param>
        /// <returns>The view of ListDisciplines.</returns>
        public async Task<IActionResult> ListDisciplines(string name, string faculty, string department)
        {
            var disciplines = await _disciplineStorage.GetAllDisciplines();
            if (!string.IsNullOrEmpty(name))
            {
                disciplines = disciplines.Intersect(disciplines.Where(p => p.Name.Contains(name))).ToList();
            }

            if (!string.IsNullOrEmpty(faculty))
            {
                disciplines = disciplines.Intersect(disciplines.Where(p => p.Faculty.Contains(faculty))).ToList();
            }

            if (!string.IsNullOrEmpty(department))
            {
                disciplines = disciplines.Intersect(disciplines.Where(p => p.Department.Contains(department))).ToList();
            }

            ListDisciplinesViewModel viewModel = new ListDisciplinesViewModel
            {
                Disciplines = disciplines.ToList(),
            };
            return this.View(viewModel);
        }

        /// <summary>
        /// Get method for view with list of discipline availabilities.
        /// </summary>
        /// <param name="model">ViewModel with values for filtring.</param>
        /// <returns>The view of ListDisciplineAvailabilities.</returns>
        public async Task<IActionResult> ListDisciplineAvailabilities(ListDisciplineAvailabilitiesViewModel model)
        {
            var disciplineAvailabilities = await _disciplineAvailabilityStorage.GetAvailabilities();
            if (!string.IsNullOrEmpty(model.Lecturer))
            {
                disciplineAvailabilities = disciplineAvailabilities.Intersect(disciplineAvailabilities.Where(p => p.Lecturer.Name.Contains(model.Lecturer))).ToList();
            }

            if (!string.IsNullOrEmpty(model.Name))
            {
                disciplineAvailabilities = disciplineAvailabilities.Intersect(disciplineAvailabilities.Where(p => p.Discipline.Name.Contains(model.Name))).ToList();
            }

            if (model.Year != null && model.Year != 0)
            {
                disciplineAvailabilities = disciplineAvailabilities.Intersect(disciplineAvailabilities.Where(p => p.Year == model.Year)).ToList();
            }

            if (model.Semester != null && model.Semester != 0)
            {
                disciplineAvailabilities = disciplineAvailabilities.Intersect(disciplineAvailabilities.Where(p => p.Semester == model.Semester)).ToList();
            }

            ListDisciplineAvailabilitiesViewModel viewModel = new ListDisciplineAvailabilitiesViewModel
            {
                DisciplineAvailabilities = disciplineAvailabilities,
            };
            return this.View(viewModel);
        }

        /// <summary>
        /// Get method for view with list disciplines of student.
        /// </summary>
        /// <param name="id">Id of student.</param>
        /// <returns>The view of ListDisciplinesOfStudent.</returns>
        public async Task<IActionResult> ListDisciplinesOfLecturer(string id)
        {
            var lecturer = await _lecturerStorage.GetLecturer(id);
            var disciplineAvailabilities = await _disciplineAvailabilityStorage.GetAvailabilities(lecturer);

            var viewModel = new ListDisciplinesOfLecturerViewModel
            {
                DisciplineAvailabilities = disciplineAvailabilities,
                Name = lecturer.Name,
            };
            return this.View(viewModel);
        }

        /// <summary>
        /// Get method for view with list disciplines of student.
        /// </summary>
        /// <param name="id">Id of student.</param>
        /// <returns>The view of ListDisciplinesOfStudent.</returns>
        public async Task<IActionResult> ListDisciplinesOfStudent(string id)
        {
            var student = await _studentStorage.GetStudent(id);
            var disciplineAvailabilities = await _disciplineAvailabilityStorage.GetAvailabilities(student);

            var viewModel = new ListDisciplinesOfStudentViewModel
            {
                DisciplineAvailabilities = disciplineAvailabilities,
                Name = student.Name,
                Group = student.Group,
            };
            return this.View(viewModel);
        }

        /// <summary>
        /// Get method for view with list of lecturers.
        /// </summary>
        /// <param name="name">Name of lecturer for filtring.</param>
        /// <param name="faculty">Faculty of lecturer for filtring.</param>
        /// <param name="department">Department of lecturer for filtring.</param>
        /// <returns>The view of ListLecturers.</returns>
        public async Task<IActionResult> ListLecturers(string name, string faculty, string department)
        {
            var lecturers = await _lecturerStorage.GetAllLecturers();
            if (!string.IsNullOrEmpty(name))
            {
                lecturers = lecturers.Intersect(lecturers.Where(p => p.Name.Contains(name))).ToList();
            }

            if (!string.IsNullOrEmpty(faculty))
            {
                lecturers = lecturers.Intersect(lecturers.Where(p => p.Faculty.Contains(faculty))).ToList();
            }

            if (!string.IsNullOrEmpty(department))
            {
                lecturers = lecturers.Intersect(lecturers.Where(p => p.Department.Contains(department))).ToList();
            }

            ListLecturersViewModel viewModel = new ListLecturersViewModel
            {
                Lecturers = lecturers,
            };
            return this.View(viewModel);
        }

        /// <summary>
        /// Get method for view with list of students.
        /// </summary>
        /// <param name="model">ViewModel with filter values.</param>
        /// <returns>The view of ListStudents.</returns>
        public async Task<IActionResult> ListStudents(ListStudentsViewModel model)
        {

            var students = await _studentStorage.GetStudents("student");

            if (!string.IsNullOrEmpty(model.Name))
            {
                students = students.Intersect(students.Where(p => p.Name.Contains(model.Name))).ToList();
            }

            if (!string.IsNullOrEmpty(model.Speciality))
            {
                students = students.Intersect(students.Where(p => p.Speciality.Contains(model.Speciality))).ToList();
            }

            if (!string.IsNullOrEmpty(model.Department))
            {
                students = students.Intersect(students.Where(p => p.Department.Contains(model.Department))).ToList();
            }

            if (!string.IsNullOrEmpty(model.Group))
            {
                students = students.Intersect(students.Where(p => p.Group.Contains(model.Group))).ToList();
            }

            if (model.Course != null && model.Course != 0)
            {
                students = students.Intersect(students.Where(p => p.Course == model.Course)).ToList();
            }

            ListStudentsViewModel viewModel = new ListStudentsViewModel
            {
                Students = students,
            };
            return this.View(viewModel);
        }

        /// <summary>
        /// Get method for view with list students of disciplineAvailability.
        /// </summary>
        /// <param name="id">Id of discipline availability.</param>
        /// <returns>The view of ListStudentsOfDisciplineAvailability.</returns>
        public async Task<IActionResult> ListStudentsOfDisciplineAvailability(string id)
        {
            var availability = await _disciplineAvailabilityStorage.GetAvailability(id);
            var students = await _studentStorage.GetStudents(new Guid(id));

            ListStudentsOfDisciplineAvailabilitiesViewModel viewModel = new ListStudentsOfDisciplineAvailabilitiesViewModel
            {
                Name = availability.Discipline.Name,
                Semestr = availability.Semester,
                Year = availability.Year,
                Students = students,
            };
            return this.View(viewModel);
        }
    }
}
