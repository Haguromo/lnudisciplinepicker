﻿namespace DisciplinePicker.Core
{
    public interface IEnvironmentProvider
    {
        string EnvironmentName { get; }
    }
}
