﻿namespace DisciplinePicker.Core
{
    public interface IRootDirectoryProvider
    {
        string RootDirectory { get; }
    }
}
