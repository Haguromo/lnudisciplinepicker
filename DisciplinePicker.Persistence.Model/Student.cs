﻿// <copyright file="Student.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System;
using System.Collections.Generic;

namespace DisciplinePicker.Persistence.Model
{
    /// <summary>
    /// Represents student.
    /// </summary>
    public class Student
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Student"/> class.
        /// </summary>
        public Student()
        {
            DisciplineChoises = new HashSet<DisciplineChoise>();
        }

        /// <summary>
        /// Gets or sets universal id of student.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets name of student.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets password of student.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets login of student.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Gets or sets group of student.
        /// </summary>
        public string Group { get; set; }

        /// <summary>
        /// Gets or sets course of student.
        /// </summary>
        public int Course { get; set; }

        /// <summary>
        /// Gets or sets peciality of student.
        /// </summary>
        public string Speciality { get; set; }

        /// <summary>
        /// Gets or sets department of student.
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// Gets or sets photo of student.
        /// </summary>
        public byte[] Photo { get; set; }

        /// <summary>
        /// Gets or sets id of role.
        /// </summary>
        public Guid RoleId { get; set; }

        /// <summary>
        /// Gets or sets role of student.
        /// </summary>
        public virtual Role Role { get; set; }

        /// <summary>
        /// Gets or sets discipline availabilities of student.
        /// </summary>
        public virtual ICollection<DisciplineChoise> DisciplineChoises { get; set; }
    }
}
