﻿// <copyright file="DisciplineChoise.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System;

namespace DisciplinePicker.Persistence.Model
{
    /// <summary>
    /// Represent a many to many communication table for discipline availabilities and students.
    /// </summary>
    public class DisciplineChoise
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DisciplineChoise"/> class.
        /// </summary>
        public DisciplineChoise()
        {
        }

        /// <summary>
        /// Gets or sets universal identificator of discipline choise.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets discipline availability id.
        /// </summary>
        public Guid DisciplineAvailabilityId { get; set; }

        /// <summary>
        /// Gets or sets student id.
        /// </summary>
        public Guid StudentId { get; set; }

        /// <summary>
        /// Gets or sets discipline availability.
        /// </summary>
        public virtual DisciplineAvailability DisciplineAvailability { get; set; }

        /// <summary>
        /// Gets or sets student.
        /// </summary>
        public virtual Student Student { get; set; }
    }
}
