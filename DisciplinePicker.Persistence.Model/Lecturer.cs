﻿// <copyright file="Lecturer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System;
using System.Collections.Generic;

namespace DisciplinePicker.Persistence.Model
{
    /// <summary>
    /// Represents lecturer.
    /// </summary>
    public class Lecturer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Lecturer"/> class.
        /// </summary>
        public Lecturer()
        {
            DisciplineAvailabilities = new HashSet<DisciplineAvailability>();
        }

        /// <summary>
        /// Gets or sets universal id of lecturer.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets name of lecturer.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets department of lecturer.
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// Gets or sets faculty of lecturer.
        /// </summary>
        public string Faculty { get; set; }

        /// <summary>
        /// Gets or sets disciplines of lecturer.
        /// </summary>
        public virtual ICollection<DisciplineAvailability> DisciplineAvailabilities { get; set; }
    }
}
