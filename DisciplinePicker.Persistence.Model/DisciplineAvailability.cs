﻿// <copyright file="DisciplineAvailability.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System;
using System.Collections.Generic;

namespace DisciplinePicker.Persistence.Model
{
    /// <summary>
    /// Represent a discipline availability.
    /// </summary>
    public class DisciplineAvailability
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DisciplineAvailability"/> class.
        /// </summary>
        public DisciplineAvailability()
        {
            DisciplineChoises = new HashSet<DisciplineChoise>();
        }

        /// <summary>
        /// Gets or sets universal identificator of discipline availability.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets year of discipline availability.
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Gets or sets semestr of discipline availability.
        /// </summary>
        public int Semester { get; set; }

        /// <summary>
        /// Gets or sets hours of discipline availability.
        /// </summary>
        public int Hours { get; set; }

        /// <summary>
        /// Gets or sets max count alloved students of discipline availability.
        /// </summary>
        public int MaxAllovedStudents { get; set; }

        /// <summary>
        /// Gets or sets min count alloved students of discipline availability.
        /// </summary>
        public int MinAllovedStudents { get; set; }

        /// <summary>
        /// Gets or sets discipline id of discipline availability.
        /// </summary>
        public Guid DisciplineId { get; set; }

        /// <summary>
        /// Gets or sets discipline of discipline availability.
        /// </summary>
        public virtual Discipline Discipline { get; set; }

        /// <summary>
        /// Gets or sets lecturer id of discipline availability.
        /// </summary>
        public Guid LecturerId { get; set; }

        /// <summary>
        /// Gets or sets lecturer of discipline availability.
        /// </summary>
        public virtual Lecturer Lecturer { get; set; }

        /// <summary>
        /// Gets or sets students of discipline availability.
        /// </summary>
        public virtual ICollection<DisciplineChoise> DisciplineChoises { get; set; }
    }
}
