﻿// <copyright file="Role.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System;
using System.Collections.Generic;

namespace DisciplinePicker.Persistence.Model
{
    /// <summary>
    /// Represents role of users.
    /// </summary>
    public class Role
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Role"/> class.
        /// </summary>
        public Role()
        {
            Students = new List<Student>();
        }

        /// <summary>
        /// Gets or sets universal id of role.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets name of role.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets students of role.
        /// </summary>
        public List<Student> Students { get; set; }
    }
}
