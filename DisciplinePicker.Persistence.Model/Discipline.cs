﻿// <copyright file="Discipline.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System;
using System.Collections.Generic;

namespace DisciplinePicker.Persistence.Model
{
    /// <summary>
    /// Class of discipline.
    /// </summary>
    public class Discipline
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Discipline"/> class.
        /// </summary>
        public Discipline()
        {
            DisciplineAvailabilities = new HashSet<DisciplineAvailability>();
        }

        /// <summary>
        /// Gets or sets universal id of discipline.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets name of discipline.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description of discipline.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets description of discipline.
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// Gets or sets description of discipline.
        /// </summary>
        public string Faculty { get; set; }

        /// <summary>
        /// Gets or sets collection discipline availabilities.
        /// </summary>
        public virtual ICollection<DisciplineAvailability> DisciplineAvailabilities { get; set; }
    }
}
